(in-package :wisp-mvc)

;; (asdf :wispylisp) 
;; start aserve
(start :port 2002)
;; easier debug
(net.aserve::debug-on :notrap)
;; html/css/javascript depend on :invert
(setf (readtable-case *readtable*) :invert)



(deform stack-calculator (stack transcript)
  ;; The fields of the form.
  (+ - * / push new-number)
  ;; When user submit the form, the following expression is evaluated
  ;; and the value(s) returned to the caller's continuation.
  (let ((op (^symbol (find-if-not #'null (list + - * / push)))))
    (if push
	(list op (cons (parse-integer new-number) stack))
	(list op stack)))
  ;; outputs the html for the form. 
  (:input :name new-number)
  (:input :name push :type 'submit :value 'push) (:br)
  (:input :name + :type 'submit :value '+)
  (:input :name - :type 'submit
	  ;; Ignore this comment. I have it here to remind myself.
	  ;; '- doesn't output anything because dash is used for the conversion
	  ;; `foo-bar' => `fooBar'. 
	  :value "-")
  (:input :name * :type 'submit :value '*)
  (:input :name / :type 'submit :value '/)
  (:div :id 'stack :style (:border 1px solid $FF0000 :width 200px)
	(dolist (number stack)
	  (html number (:br))))
  (:div :id 'transcript :style (:border 1px solid $00FF00 :width 200px)
	(dolist (cmd transcript)
	  (html cmd (:br)))))



#+(or)
(defwethod calculator-loop (&optional stack)
  (let* ((op/stack (read-form 'stack-calculator stack))
	 (op (car op/stack))
	 (new-stack (cadr op/stack))) 
    (if (eql op #'push)
	;; Wethod is wrapped in with-call/cc, so it's tail-recursive... I think.
	(calculator-loop new-stack)
	(calculator-loop (cons
			  (funcall op (first new-stack) (second new-stack))
			  (nthcdr 2 new-stack))))))


(defwethod calculator-loop () 
  (loop
     with transcript
     for (op stack) =
     ;; `read-form' will output the html for the form, save the continuation,
     ;; and return immediately from `calculator-loop'. When the user submits the
     ;; form, the url-handler calls the continuation.
     ;;
     ;; To the programmer, it appears as though read-form returns (list op stack).
       (read-form 'stack-calculator nil nil) then (read-form 'stack-calculator stack transcript)
     do (if (eql op 'push)
	    (push (list 'push (car stack)) transcript)
	    (let ((result (funcall (symbol-function op) (second stack) (first stack))))
	      (push (format nil "~S => ~S"
			    (list op (second stack) (first stack))
			    result)
		    transcript)
	      (setf stack (cons result (nthcdr 2 stack)))))))





(defurlmap wisp-sys
  ;; call-from-k takes the string in place of  `:k-id' as its (single) argument. 
  ("form-k/:k-id" :handler call-form-k))

(defurlmap calculator
  ;; maps the url /calc to the wethod calculator-loop
  ("calc" :wethod calculator-loop))


;; (get-effective-urls 'calculator) 

;; ((:wethod (:url "/calc" "^/calc") :dispatch
;;   #<standard-generic-function calculator-loop> :discriminators (nil)
;;   :wethod-urls (("" "^$"))))


;; (get-effective-urls 'wisp-sys)

;; ((:handler (:url "/form-k/:k-id" "^/form-k/([^/]+)$") :dispatch
;;   #<Interpreted Function CALL-FORM-K>))




(attach-urlmap "test" 'calculator)
(attach-urlmap "wisp-sys" 'wisp-sys)

;; now try http://localhost:2002/test/calc















;; What follows is a transcript of what happens behind the scene.
#|

(calculator-loop)
;; note that the continuation number is 9. ("/wisp-sys/form-k/9")
;; corresponding to ("form-k/:k-id" :handler call-form-k)

<form action='/wisp-sys/form-k/9' method='post'>
  <input name='(stack-calculator new-number)'>
  <input name='(stack-calculator push)' type='submit' value='push'>
  <br>
  <input name='(stack-calculator +)' type='submit' value='+'>
  <input name='(stack-calculator -)' type='submit' value='-'>
  <input name='(stack-calculator *)' type='submit' value='*'>
  <input name='(stack-calculator /)' type='submit' value='/'>
  <div id='stack' style='border: 1px solid #FF0000 ;
  width: 200px ;'></div>
  <div id='transcript' style='border: 1px solid #00FF00 ;
  width: 200px ;'></div>
</form>





(let ((*parameters* `(("(stack-calculator push)" . "push")
		      ("(stack-calculator new-number)" . "10"))))
  (call-form-k "9"))

<form action='/wisp-sys/form-k/10' method='post'>
  <input name='(stack-calculator new-number)'>
  <input name='(stack-calculator push)' type='submit' value='push'>
  <br>
  <input name='(stack-calculator +)' type='submit' value='+'>
  <input name='(stack-calculator -)' type='submit' value='-'>
  <input name='(stack-calculator *)' type='submit' value='*'>
  <input name='(stack-calculator /)' type='submit' value='/'>
  <div id='stack' style='border: 1px solid #FF0000 ;
  width: 200px ;'>10
  <br>
  </div>
  <div id='transcript' style='border: 1px solid #00FF00 ;
  width: 200px ;'>(push 10)
  <br>
  </div>
</form>



(let ((*parameters* `(("(stack-calculator push)" . "push")
		      ("(stack-calculator new-number)" . "20"))))
  (call-form-k "10"))

<form action='/wisp-sys/form-k/12' method='post'>
  <input name='(stack-calculator new-number)'>
  <input name='(stack-calculator push)' type='submit' value='push'>
  <br>
  <input name='(stack-calculator +)' type='submit' value='+'>
  <input name='(stack-calculator -)' type='submit' value='-'>
  <input name='(stack-calculator *)' type='submit' value='*'>
  <input name='(stack-calculator /)' type='submit' value='/'>
  <div id='stack' style='border: 1px solid #FF0000 ;
  width: 200px ;'>20
  <br>
  10
  <br>
  </div>
  <div id='transcript' style='border: 1px solid #00FF00 ;
  width: 200px ;'>(push 20)
  <br>
  (push 20)
  <br>
  (push 10)
  <br>
  </div>
</form>


(let ((*parameters* `(("(stack-calculator +)" . "+")
		      ("(stack-calculator new-number)" . "20"))))
  (call-form-k "12"))

<form action='/wisp-sys/form-k/13' method='post'>
  <input name='(stack-calculator new-number)'>
  <input name='(stack-calculator push)' type='submit' value='push'>
  <br>
  <input name='(stack-calculator +)' type='submit' value='+'>
  <input name='(stack-calculator -)' type='submit' value='-'>
  <input name='(stack-calculator *)' type='submit' value='*'>
  <input name='(stack-calculator /)' type='submit' value='/'>
  <div id='stack' style='border: 1px solid #FF0000 ;
  width: 200px ;'>30
  <br>
  </div>
  <div id='transcript' style='border: 1px solid #00FF00 ;
  width: 200px ;'>(+ 10 20) =&gt; 30
  <br>
  (push 20)
  <br>
  (push 20)
  <br>
  (push 10)
  <br>
  </div>
</form>

|#