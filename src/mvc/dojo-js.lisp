(in-package :wisp-mvc)

(defun ^dojo-package-string (dojo-package)
  (etypecase dojo-package
    (symbol (^html-string dojo-package))
    (string dojo-package)))

(defun ^html-string (thing)
  (etypecase thing
    (symbol (string-invert (symbol-name thing)))
    (string thing)))

(deftag :js-require (&rest dojo-packages)
  `(:script :type "text/javascript" (js (prog
					    ,@(mapcar (fn (dojo-package)
							`(dojo.require ,(^dojo-package-string dojo-package)))
						      dojo-packages)))))

(deftag :js (&rest body)
  `(:script :type "text/javascript" (js (prog ,@body))))

(deftag :js-src (&rest sources)
  `(:progn ,@(mapcar (fn (source)
		       `(:script :type "text/javascript" :src ,(^html-string source)))
		     sources)))

(deftag :js-init (&rest body)
  `(:js (function init () ,@body)
	(dojo.addOnLoad init)))

(deftag :js-down (&rest body)
  `(:js (function clean-up () ,@body)
	(dojo.addUnLoad clean-up)))

