;; Shit. I just realized that I am a complete retard. The matching
;; process has linear runtime wrt to the number of possible matches... 
;;
;; TODO Fix linear run-time.
;; It's hard... I have to use pattern matching, yet garuantee some
;; sort of linear ordering. Wait till later.



;; "/pk/qux/number_1/number_2/number_3" 
;; ("pk" "qux" ("number" "1") ("number" "2") ("number" "3"))

;; (nil nil arg1 arg2 arg3)

;; "/wethod/class1_arg1/class2_arg2" 
;; ("wethod" ("class1" "arg1") ("class2" "arg2"))
;; (nil arg1 arg2)

;; "/fn/arg1/arg2"
;; ("fn" "arg1" "arg2")
;; (nil arg1 arg2)
;; ;; => "arg1", "arg2"

;; "/fn/arg1/arg2"
;; ("fn" "arg1" "arg2")
;; (arg1 arg2 arg3)
;; ;; => "fn", "arg1", "arg2"


(in-package :wisp-mvc)


(defun ^url-dispatcher (url-prefix urlmap)
  (if *wisp-debug*
      (fn (*request* *entity*)
	;; recreate the dispatcher so I don't have to re-attach the
	;; urlmap everytime I modify %url-dispatcher fo easier debugging.
	(funcall (curry #'%url-dispatcher url-prefix urlmap *request* *entity*)))
      (fn (*request* *entity*)
	(%url-dispatcher url-prefix urlmap *request* *entity*))))

(defun %url-dispatcher (url-prefix urlmap *request* *entity*)
  (declare (special *request*) (special *entity*)) 
  ;; setup the dynamic enviornment that allows functional interface to http request.
  ;; *cookies*
  ;; *parameters*
  ;; *session*
  ;;
  ;; Be *really* careful about package issues. A wethod will set
  ;; *package* to be the package where it's defined. In general,
  ;; things should work as expected as long as all http-data stuff
  ;; are strings. 
  (let ((*parameters* (parse-request-parameters (request-query *request*)))
	(*readtable* *readtable*)) 
    (declare (special *parameters*))
    (setf (readtable-case *readtable*) :invert)
    #+(or)(print "fuck this")
    (handler-case 
	(match-url-dispatch 
	 (remove-url-prefix (net.uri:uri-path (request-uri *request*)) url-prefix)
	 urlmap)
      (werror (c)
	(render-error c)))))

(defun remove-url-prefix (url url-prefix)
  (=~ url-prefix url $a))


(defparameter *parameters* nil)

(defun parse-request-parameters (alist) 
  alist)


(defun param (key)
  ;; this is awful... 
  (cdr (assoc (^string key) *parameters*
	      :test #'equal)))







(defun match-url-dispatch (request-url urlmap &key (nested? nil))
  "Matches the request url against urlmap, then apply the dispatch function."
  ;; expects the request-url not be prepended with `/'
  (with-slots (handlers wethods class-wethods packages nested-maps) urlmap
    (acond2
     ((match-url-handlers request-url handlers) (values it t))
     ((match-url-methods request-url (append class-wethods wethods)) (values it t))
     ((match-url-packages request-url packages) (values it t))
     ((match-nested-urlmaps request-url nested-maps :nested? nested?) (values it t)) 
     ((not nested?) (error 'wisp-no-dispatch-for-url :url request-url)))))



(defun match-url-handlers (request-url url-handlers)
  ;; if no match, returns nil, which is the same as (values nil nil),
  ;; so it works just fine for acond2 in `match-url-dispatch'.
  (loop
     for url-handler in url-handlers
     for (match args) = (multiple-value-list (scan-to-strings (url-regex url-handler) request-url))
     when match do (return (values (apply-url-handler url-handler args) t))))

(defun match-url-methods (request-url url-methods)
  (loop
     for url-method in url-methods
     for (match args discriminator) = (match-url-method (=~ (url-regex url-method) request-url $a) url-method)
     when match do(return (values (apply-url-method url-method args discriminator)))))

(defun match-url-method (request-url url-method)
  (loop
     ;; discriminators and wethod-url-regexes should be the same length.
     for discriminator in (discriminators url-method)
     for wethod-url-regex in (wethod-url-regexes url-method)
     for (match args) = (multiple-value-list (scan-to-strings wethod-url-regex request-url))
     when match do (return (list match args discriminator))))

(defun match-url-packages (request-url url-packages)
  ;; to make my life easier, don't try to dynamically update a package map yet.
  ;; Though this would be nice to have. 
  (loop
     for url-package in url-packages
     for (val match?) =
     (multiple-value-list
      (with-slots (url-regex urlmap) url-package
	(match-url-dispatch (=~ url-regex request-url $a) urlmap :nested? t)))
     when match? do (return (values val t)))) 

(defun match-nested-urlmaps (request-url url-nested-maps &key (nested? nil))
  (flet ((match-rec (map &key nested?)
	   (with-slots (url-regex nested-map-name) map
	     (awhen (=~ url-regex request-url $a)
	       (match-url-dispatch it (get-urlmap nested-map-name) :nested? nested?)))))
    (dolist (map (butlast url-nested-maps))
      (awhen2 (match-rec map :nested? t)
	(return-from match-nested-urlmaps (values it t))))
    ;; tail-recurse to the last nested urlmap
    (aand (last1 url-nested-maps) (match-rec it :nested? nested?))))



(defun apply-url-handler (url-handler args)
  (apply (dispatch url-handler) (map 'list #'identity args)))

(defgeneric apply-url-method (url-method args discriminator)
  (:documentation "Applies args (converted to lisp data by `^wisp-arg') found in the url to the method function."))


(defmethod apply-url-method ((url-method url-wethod) args discriminator)
  (apply (dispatch url-method)
	 (map 'list
	      (fn (type arg) (^wisp-arg type arg))
	      discriminator args)))

;; No idea how to implement class wethods yet.
;; (defmethod apply-url-method ((url-method url-class-wethod) args discriminator)
;;   (apply (dispatch url-method)
;; 	 (map 'list
;; 	      (fn (type arg) (^wisp-arg
;; 			      ;; the specializer of class-wethod is stored in the form (eql <class-symbol>)
;; 			      (find-class (second type))
;; 			      arg))
;; 	      discriminator args)))



(defgeneric ^wisp-arg (type url-arg)
  (:documentation "Used by url dispatcher to convert url string to lisp data."))

(defmethod ^wisp-arg ((type t) url-arg)
  "Default converstion from url-string to lisp data is the url-string itself."
  url-arg)
