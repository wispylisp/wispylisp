(in-package :wisp-mvc)


(defmacro deform (name fn-list values handler &rest html)
  ;; use rkr macro? 
  `(setf (gethash ,(^string name) *wisp-forms*)
	 (make-instance 'wisp-form
			:handler-fn ,(make-form-handler name fn-list values handler)
			:html-fn ,(make-form-html-fn name fn-list values html))))


(defclass wisp-form ()
  ((handler-fn :initarg :handler-fn :reader handler-fn)
   (html-fn :initarg :html-fn :reader html-fn))) 


(defparameter *wisp-forms* (make-hash-table :test #'equal))

(defun get-wisp-form (name)
  (gethash (^string name) *wisp-forms*))


;; closure for form handler
(defun make-form-handler (form-name fn-list values handler) 
  (let ((k-id (gensym "k-id")))
    ;; is it a good idea to introduce anaphoric for k-id?
    `(fn ,(append (list 'k-id) fn-list)
       ,(with-form-values form-name values handler))))

(defun with-form-values (form-name values handler)
  `(let (,@(mapcar (fn (value)
		    `(,value (param '(,form-name ,value))))
		  values))
     ,handler))

       

(defun get-form-value (form-name value)
  (list form-name value))




;; closure for form html
(defun make-form-html-fn (form-name fn-list values html)
  (let ((form-handler-url (gensym "handler-url")))
    `(fn ,(append (list 'k-id) fn-list)
       ,(with-form-field-names form-name values
			       (make-wisp-form html)))))


(defun with-form-field-names (form-name values html)
  ;; what's the point of accumulating the fields of a form into one hash, as in RoR? 
  `(let (,@(mapcar (fn (value)
		    `(,value (strcat "(" ',form-name " " ',value ")")))
		  values))
     ,html))

(defun make-wisp-form (html)
  `(html
     (:wisp-form :action (make-form-k-url k-id)
		 ;; or I can specify a keyword argument :form-handler-url in the official
		 ;; interface to the read-form macro.
		 :method 'post
		 ,@html)))

(defun make-form-k-url (k-id)
  ;; Don't know how to make the handler url dynamically configurable.
  (strcat "/wisp-sys/form-k/" (^string k-id)))