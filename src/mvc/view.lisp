(in-package :wisp-mvc)



(defparameter *defined-views* (make-hash-table :test #'equal))
(defparameter *html-output-stream* *standard-output*)
(defparameter *request* nil)

;; (defmacro respond-with-html ((stream-name) &body body)
;;   `(let ((html-response
;; 	  (let ((,stream-name (make-string-output-stream)))
;; 	    (declare (special ,stream-name))
;; 	    (unwind-protect 
;; 		 (handler-case  (progn ,@body (get-output-stream-string ,stream-name))
;; 		   (werror (c)
;; 		     ;; bind a fresh output stream
;; 		     (close ,stream-name) (setf ,stream-name (make-string-output-stream))
;; 		     (fmt :> ,stream-name
;; 			  "Unhandled Error Signaled." :%) 
;; 		     (print c ,stream-name) 
;; 		     (get-output-stream-string ,stream-name)))
;; 	      (close ,stream-name))))) 
;;      ;; probably can use copy-stream instead.
;;      (if *request*
;; 	 ;; if is serving a html request
;; 	 (with-http-response (*request* *entity*) 
;; 	   (with-http-body (*request* *entity*) 
;; 	     (let ((out (request-reply-stream *request*))) 
;; 	       (write-string html-response out))))
;; 	 ;; or is debugging in lisp
;; 	 (write-string html-response *standard-output*))))

(defmacro respond-with-html ((stream-name) &body body)
  ;; TODO put the body in a flet.
  `(progn
     (if *request*
	 ;; if is serving a html request
	 (no-call/cc
	  (with-http-response (*request* *entity*) 
	    (with-http-body (*request* *entity*)
	      ;; there really shouldn't be application errors when rendering the output.
	      ;; If error IS signaled, I have no idea how aserve will behave.
	      (let ((,stream-name (request-reply-stream *request*))) 
		,@body))))
	 ;; or is debugging in lisp
	 (let ((,stream-name *standard-output*))
	   (declare (special ,stream-name))
	   ,@body))
     t))
  



(defun parse-method-definition (definition)
  (multiple-value-bind (qualifiers fn-list/body) (split-if (compose #'not #'keywordp) definition) 
    (multiple-value-bind (required-args other-args) (split-if
						     (fn (arg)
						       (and (atom arg)
							    (equal (char (symbol-name arg) 0) #\&)))
						     (car fn-list/body))
    (values qualifiers required-args other-args (cdr fn-list/body))))) 

(defmacro defview (name &body definition)
  (multiple-value-bind (qualifiers required-args other-args body) (parse-method-definition definition) 
    (let* ((fn-name (symcat 'view- name))
	   (def-generic (if (fboundp fn-name)
			    nil
			    ;; make the generic function for the view
			    (let ((untyped-fnlist (mapcar (fn (arg-spec)
							    (if (consp arg-spec)
								(car arg-spec)
								arg-spec))
							  required-args)))
			      `(defgeneric ,fn-name (,@untyped-fnlist &allow-other-keys)))))) 
      `(progn
	 ,@(if def-generic
	       (list def-generic
		     `(setf (gethash ',(symbol-name fn-name) *defined-views*)
			    (symbol-function ',fn-name))))
	 
	 (defmethod
	     ,fn-name
	     ,@qualifiers
	   ,(append required-args other-args) 
	   (html ,@body))))))

(defun find-view (name)
  (gethash (symbol-name (symcat 'view- name)) *defined-views*)) 

;; All variants of "render" should only be called once per request.
(defun render-view (name &rest rest)
  (apply (aif (find-view name) it
	      (error 'wisp-undefined-view :view-name name))
	 rest))


(defmacro render-html (&body body) 
  `(respond-with-html (*html-output-stream*)
     (with-foo-output (*html-output-stream*)
       (html ,@body))))


(defgeneric render-error (werror))

(defmethod render-error ((werror werror))
  (render-html (:html (:title "Error. " werror)
		      (:body (:div "Unhandled error: " werror)))))

;; (defview object->html ((obj wisp-object))
;;   (let* ((class (class-of obj))
;; 	 ;; I was hoping to use the existence of reader and writer as flag for permission of read/write
;; 	 #+(or)(slot-readers (mapcar
;; 			      (compose #'car #'slot-definition-readers)
;; 			      (compute-slots class)))
;; 	 (slot-names (mapcar #'slot-definition-name (compute-slots class)))) 
;;     (html (:b (:i class))
;; 	  (:table (:tr
;; 		   (dolist (slot-name slot-names)
;; 		     (html (:th slot-name))))
;; 		  (:tr
;; 		   (dolist (slot-name slot-names)
;; 		     (html (:td (if (slot-boundp obj slot-name) 
;; 				    (html (:i (:print (slot-value obj slot-name))))
;; 				    nil)))))))))



(deftag :template (template-name &other-keys others &body body)
  ;; the choice of which template to use is static.
  ;; I can't think of a good way to make this dynamic.
  (if (keywordp template-name)
      (apply (get-template-fn template-name) (append others body))
      (error "No dynamic templating... sorry.")))

(defparameter *wisp-templates* (make-hash-table :test #'eql))

(defun get-template-fn (template-name)
  (gethash (^keyword template-name) *wisp-templates*))

(defun save-template-fn (template-name fn)
  (setf (gethash (^keyword template-name) *wisp-templates*)
	fn)) 

(def/rkr/macro deftemplate (template-name fnlist &rest body) 
  `(save-template-fn ',template-name
		     (rkr/fn ,fnlist ,@body)))


;; (deftemplate foo (&key (header "bullshit")
;; 		       ;; kinda tasteless to provide defaults this way... 
;; 		       (body '(:div "the default body"))
;; 		       (time "To Die"))
;;   ;; the above alternative is impossible to implement.
;;   `(let ((time ,time))
;;     (html
;;       (:html
;; 	(:body time
;; 	       (:div ,header)
;; 	       (:div ,body)
;; 	       time)))))

;; (html (:template :foo))





;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

