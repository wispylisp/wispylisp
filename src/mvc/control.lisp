;; It might be interesting to have a `with-state' macro that
;; automatically clean-up all the continuations invovled after existing
;; the dynamic extent. The name `stately' is kinda interesting.

(in-package :wisp-mvc)

(defmacro test-wethod (form))


;; (defparameter *wisp-read-form-template-fns* 
;;   (make-hash-table :test #'equal))

;; (defun get-read-form-template-fn (k-id)
;;   (gethash k-id *wisp-read-form-template-fns*))

;; (defun save-read-form-template-fn (k-id read-form-template-fn)
;;   (setf (gethash k-id *wisp-read-form-template-fns*)
;; 	read-form-template-fn))




(defparameter *wisp-ks* (make-hash-table :test #'equal))
(defparameter *k-id* nil
  "The id of the running continuation.")

(let ((counter 0))
  (defun fresh-k-id (method-name form-name)
    (princ-to-string (incf counter))))

(defun save-wisp-k (k-id k type &rest data) 
  (setf (gethash k-id *wisp-ks*) (list* k type *package* data)))

(defun get-wisp-k (k-id) 
  (gethash k-id *wisp-ks*))

(defun call-wisp-k (k-id &rest values)
  ;; probably want to trap form errors here.
  (destructuring-bind (k type package . data) (get-wisp-k k-id)
    (let ((*package* package)
	  (*k-id* k-id))
      (unless k (error 'wisp-invalid-continuation-id))
      (case type
	(:form
	 (let ((mvlist (multiple-value-list (funcall (car data)))))
	   (if (cdr mvlist)
	       (apply #'kall k mvlist)
	       (funcall #'kall k (car mvlist)))))
	(t (apply #'kall (cons k values)))))))





(defun save-form-k (k-id k handler-fn)
  (save-wisp-k k-id k :form handler-fn))

(defun call-form-k (k-id)
  (call-wisp-k k-id))









(defparameter *package-wethods* (make-hash-table :test #'equal))

(defun save-wethod (wethod-symbol &optional (package *package*))
  (let ((wethods-in-package (gethash (^keyword package) *package-wethods*)))
    (setf wethods-in-package (adjoin wethod-symbol wethods-in-package))
    (setf (gethash (^keyword package) *package-wethods*) wethods-in-package)))

(defun get-direct-wethods (&optional (package *package*))
   (gethash (^keyword package) *package-wethods*))

(defun accessible? (symbol)
  (find-symbol (symbol-name symbol)))

(defun get-accessible-wethods (&optional (package *package*)) 
  (let ((packages (mapcar #'^keyword (cons package (package-use-list package)))))
    (mappend (fn (package) (filter #'accessible? (get-direct-wethods package)))
	     packages)))



(defmacro defwethod (name fn-list &body body)
  ;; TODO combine this with defwethod
  ;; add session id later.
  `(progn
     (defmethod ,name ,fn-list
       ;; The web-server doesn't know the package of this wethod. 
       (let ((*package* (find-package ,(package-name *package*))))
	 (with-call/cc 
	   (block ,name	;; block names outside with-call/cc are not visible inside.
	     (macrolet
		 ((read-form (&rest args) `(%read-form-macro ,',name ,@args))
		  (render (&rest args)
		    `(respond-with-html (*html-out*)
		       (with-foo-output (*html-out*)
			 ,(if (null args)
			      '(render-view ',name ,@(mapcar (fn (arg-spec)
							       (if (consp arg-spec)
								   (car arg-spec)
								   arg-spec))
							     fn-list))
			      `(render-view ,',name ,@args))))))
	       ,@body)))))
     (save-wethod ',name)
     ',name)) 




;; (defclass-wethod foo (class1 class2) ((a class1) (b class2) &key mu)
;;  (list class1 class2 a b mu))




;; (defmacro defclass-wethod (name classes fn-list &body body)
;;   (when (null classes) (error "Defining class-wethod on a null class list"))
;;   (let* (;; the class list is allowed to be a single symbol
;; 	 (classes (mklst classes))
;; 	 (wethod-name (apply #'symcat
;; 			    (mapcar (fn (class) (symcat class "-"))
;; 				    (mklst classes))
;; 			    (list name))))
;;     ;; is it good to define class-wethod as wethods?
;;    `(defwethod ,wethod-name ,fn-list
;;       (let (,(mapcar (fn (class) `(,class (find-class ',class)))
;; 		     classes))
;; 	,@body))))






(def/rkr/macro %read-form-macro (name &key template &rest args) 
  (let ((k (gensym "k"))
	(k-id (gensym "k-id"))
	;; This form depends on the fact that form-name is not a keyword. 
	(form-name (car args))
	(form-args (cdr args))
	#+(or)(form (gensym "form"))
	)
    `(let/cc ,k
       (let ((,k-id (fresh-k-id  ',name ,form-name))) 
	 ;; ,(when template
	 ;; 		;; how expensive is it to create call/cc closures?
	 ;; 		;; TODO memoizing actually must occur at runtime.
	 ;; 		`(save-read-form-template-fn ,k-id
	 ;; 					     ;; note this closure would capture variables.
	 ;; 					     (fn (this-form-html-string)
	 ;; 					       (symbol-macrolet ((this-form (html (:noescape this-form-html-string))))
	 ;; 						 (html ,template)))))
	 (render-form ,k-id
		      ,k
		      ,(if template
			   `(no-call/cc
			     (fn (this-form-html-string)
			       (symbol-macrolet ((this-form (html (:noescape this-form-html-string))))
				 (html ,template))))
			   nil)
		      ,form-name
		      ,@form-args)
	 (return-from ,name t)))))


;; (%read-form-macro foo 'foo-form 1 2 3)

;; (%read-form-macro moo :template (:html (:header this-form)) 'foo 'foo-form 1 2 3)

(defun wisp-k-handler (k-id)
  (call-form-handler k-id))

;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

