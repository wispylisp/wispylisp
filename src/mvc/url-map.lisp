;; THINK What does it mean for a handler to have a "" path?
;; TODO change matcher so url allows query parameters.

(in-package :wisp-mvc)

(defun request-url-explode (url)
  )

(defparameter *urlmaps* (make-hash-table :test #'equal))

(defclass urlmap ()
  ((name :initarg :name :reader urlmap-name)
   (url-prefixes :initform nil :accessor urls)

   (handlers :initform nil :initarg :handlers)
   (wethods :initform nil :initarg :wethods)
   (class-wethods :initform nil :initarg :class-wethods)
   (packages :initform nil :initarg :packages) 
   ;; store symbols
   (nested-maps :initform nil :initarg :maps)))

(defclass url-spec ()
  ((url :initarg :url :reader url)
   (url-regex :reader url-regex)))

(defclass url-handler (url-spec)
  ((dispatch :initarg :dispatch :reader dispatch)))

;; (defclass url-dispatch (url-spec)
;;    ())

(defclass url-method (url-handler)
  ((url-args :initarg :url-args :reader url-args)
   ;; don't know what I am saving discriminators for...
   (discriminators :initarg :discriminators :reader discriminators)
   (wethod-urls :initarg :wethod-urls)
   (wethod-url-regexes :initarg :wethod-url-regexes :reader wethod-url-regexes)))

(defclass url-class-wethod (url-method) ())

(defclass url-wethod (url-method) ())

(defclass url-nested-map (url-spec)
  ((nested-map-name :initarg :nested-map-name :reader url-nested-map-name)))

(defclass url-package (url-spec)
  ((package :initarg :package :reader url-package)
   (urlmap :initarg :urlmap :reader url-package-urlmap)))

(defmethod print-object ((urlmap urlmap) s)
  (print-unreadable-object (urlmap s :type t :identity t)
    (princ (urlmap-name urlmap) s)))


(defmethod initialize-instance :after ((url-spec url-spec) &key)
  (with-slots (url url-regex) url-spec
    (setf url-regex (create-url-scanner (strcat "^" url)))))


;; (defmethod initialize-instance :after ((url-spec url-method) &key) 
;;   (flet ((^wethod-url-regex (wethod-url)
;; 		 (loop for (arg-specializer arg-name) in (extract-url-fnlist url)
;; 		    collect (aif (equal arg-specializer "")
;; 				 (find-class t)
;; 				 (find-class (^symbol arg-specializer))) into ass
;; 		    collect arg-name into ans
;; 		    finally (return (list ass ans))))))
;;   (with-slots (wethod-urls wethod-url-regexes discriminators) url-spec
      
;;     ))


(defmethod initialize-instance :after ((url-spec url-handler) &key) 
  (with-slots (url url-regex) url-spec 
    (setf url-regex (make-url-handler-regex url))))

(defmethod initialize-instance :after ((url-spec url-method) &key) 
  (with-slots (url url-regex wethod-urls wethod-url-regexes) url-spec
    (setf url-regex (create-url-scanner (strcat "^" url)))
    (setf wethod-url-regexes (mapcar #'make-url-handler-regex wethod-urls))))


;; (defmethod initialize-instance :after ((url-spec url-nested-map) &key)
;;   (with-slots (url url-regex) url-spec
;;     (setf url-regex (create-url-scanner url))))


(defun create-url-scanner (url-regex)
  (if *wisp-debug*
      url-regex
      (create-scanner url-regex)))



(defun make-url-handler-regex (url-spec-string)
  ;; The very first underscore `_' separates type and arg. All
  ;; subsequent underscores are considered part of the url arg.
  ;;
  ;; actually, the end shouldn't be $, because I want to allow query parameters.
  (create-url-scanner (strcat "^" (regex-replace-all "([^/:_]+_)?:[^/]+" url-spec-string "\\1([^/]+)") "$")))


(defun extract-url-fnlist (url)
  "Parses urls consisted of <specializer>_:<arg>/ into tuples to be used for wethod dispatch."
  ;; should  "ab_c__:d/:efg/hij/_:klm" be legal?
  (let ((arg-regex (create-scanner "([^/:]+_)?:[^/]+"))
	(split-regex (create-scanner "_?:"))
	(args nil))
    (do-matches-as-strings (m arg-regex url)
      (push m args))
    (mapcar
     (fn (arg) (split split-regex arg))
     (nreverse args))))


(defun urlcat (&rest urls)
  "Concatenate urls. Empty urls like `//' are collapsed into `/'. The
resulting url will conform to the form of

/<string1>/<string2>.../<string-n>

That is, with `/' before, but not after.

Series of empty strings \"\" results in empty string. "
  (ensure-prefix-slash-only
   (regex-replace-all "/+"
		      (apply #'strcat
			     (mapcar #'ensure-prefix-slash urls))
		      "/")))




;; (ensure-prefix-slash-only "")
;; (ensure-trailing-slash-only "")

;; (ensure-trailing-slash-only "/")
;; (ensure-prefix-slash-only "/")

(defun ensure-trailing-slash-only (url)
  (=~ "^/*(.*?)/*$" url (strcat ($ 1) #\/)))

(defun ensure-prefix-slash-only (url)
  (if (equal url "")
      ""
      (=~ "^/*(.*?)/*$" url (strcat #\/ ($ 1)))))

(defun ensure-trailing-slash (url)
  (when (equal url "") (return-from ensure-trailing-slash "/"))
  (if (eql (char url (1- (length url))) #\/)
      url
      (strcat url "/"))) 

(defun ensure-prefix-slash (url)
  (when (equal url "") (return-from ensure-prefix-slash "/"))
  (if (eql (char url 0) #\/)
      url
      (strcat "/" url)))

;; (defun strip-trailing-slash (url)
;;   (when (equal url "") (return-from strip-trailing-slash url))
;;   (if (eql (char url (1- (length url))) #\/)
;;       (subseq url 0 (1- (length url)))
;;       url))

;;;; defurlmap 

(defun get-urlmap (map-name) 
  (gethash  map-name *urlmaps*))


(defun get-effective-urls (map-name)
  (awhen (get-urlmap map-name) (unparse-urlmap it)))


(defmacro defurlmap (map-name &rest url-specs) 
  ;; A request url is test against the urlmap in the order:
  ;; handler, class-wethod, wethod, package, nested-map. 
  `(register-urlmap ',map-name ,@(mapcar #'normalize-url-spec url-specs)))

(defun register-urlmap (map-name &rest url-specs)
  "Makes a urlmap available for attaching (publishing)."
  (setf (gethash map-name *urlmaps*)
	(make-urlmap map-name url-specs)))


(defun make-urlmap (map-name url-specs)
  "Stores normalized url-specs (according to type) within a urlmap object."
  (let (wethods class-wethods handlers packages maps)
    (dolist (url-spec url-specs)
      (let* ((type (first url-spec))
	     (plist (cdr url-spec))
	     (url (getf plist :url)))
	(ecase type
	  (:handler
	   (push (make-instance 'url-handler
				:url url
				:dispatch (getf plist :dispatch))
		 handlers))
	  (:wethod
	   (push (make-instance 'url-wethod
				:url url
				:dispatch (symbol-function (getf plist :wethod-name)) 
				:discriminators (getf plist :discriminators)
				:wethod-urls (getf plist :wethod-urls))
		 wethods))
	  (:class-wethod
	   (push (make-instance 'url-class-wethod
				:url url
				:dispatch (symbol-function (getf plist :wethod-name)) 
				:discriminators (getf plist :discriminators)
				:wethod-urls (getf plist :wethod-urls))
		 class-wethods))
	  (:package
	   (push (make-url-package plist) packages))
	  ;; TODO make the url path in url-spec more robust.
	  (:map (push (make-instance 'url-nested-map
				     :url url
				     :nested-map-name (getf plist :map-name))
		      maps)))))
    (make-instance 'urlmap
		   :name  map-name
		   :handlers (nreverse handlers)
		   :wethods (nreverse wethods)
		   :class-wethods (nreverse class-wethods)
		   :packages (nreverse packages)
		   :maps (nreverse maps))))


(defun make-url-dispatch (type url dispatch)
  (make-instance type :url url :dispatch dispatch))

(defun make-url-package (plist)
  (let ((url (getf plist :url))
	(package (getf plist :package))
	(package-wethods (getf plist :package-wethods)))
    (make-instance 'url-package
		   :package package
		   :url url
		   :urlmap (make-urlmap (gensym (^string package))
					package-wethods))))




(defun normalize-url-spec (url-spec)
  "Converts defurlmap forms into internal representation to be processed by `make-urlmap'."
  (let* ((handler-designator (third url-spec)) ;; a symbol or a form that returns function.
	 (handler (if (consp handler-designator)
		      handler-designator
		      `(symbol-function ',handler-designator)))
	 (url (ensure-prefix-slash-only (car url-spec)))) 
    (ecase (second url-spec)
      (:handler `(list :handler :url ,url :dispatch ,handler))
      (:wethod
       (let* ((discriminators (aif (nthcdr 3 url-spec)
				   (check-function-discriminators handler-designator it)
				   ;; when no specializers are specified, it means all are specified.
				   (get-function-discriminators handler-designator))))
	 `'(:wethod
	    :url ,url
	    :wethod-urls
	    ,(mapcar (curry #'^wethod-url-frag handler-designator)
		     discriminators)
	    :discriminators ,discriminators
	    :wethod-name ,handler-designator)))
      (:class-wethod
       (let ((discriminators (nthcdr 3 url-spec)))
	 `'(:class-wethod
	    :url ,url
	    :discriminators ,(mapcar (fn (d) (mapcar (curry #'list 'eql) d))
				     discriminators)
	    :wethod-urls
	    ,(mapcar
	      (fn (discriminator)
		;; make sure all the classes are defined. Else find-class signals error.
		(mapc #'find-class discriminator)
		(^class-wethod-url-frag handler-designator discriminator))
	      discriminators)
	    :wethod-name ,handler-designator)))
      (:package (^url-package-spec url-spec))
      (:map  `'(:map :url ,url :map-name ,(last1 url-spec))))))

(defun ^wethod-url-frag (wethod-name specializers)
  (ensure-prefix-slash-only
   (list-to-delimited-string 
    (let ((counter 0))
      (mapcar (fn (specializer)
		(if (eql specializer t)
		    (strcat ":arg" (^string (incf counter)))
		    (strcat (^string specializer) "_:arg" (^string (incf counter)))))
	      specializers))
    #\/)))

(defun ^class-wethod-url-frag (wethod-name class-specializers)
  (ensure-prefix-slash-only
   (list-to-delimited-string (mapcar '^string class-specializers)
			     #\/)))



(defun ^url-package-spec (url-spec)
  "Make wethod-url-specs for all the wethods accessible in a pacakage."
  (let ((url (ensure-prefix-slash-only (first url-spec)))
	(url-package (third url-spec))) 
    (flet ((^wethod-spec (wethod-name)
	     `(,(^string wethod-name) :wethod ,wethod-name ,@(get-function-discriminators wethod-name))))
      (let ((wethods (get-accessible-wethods url-package))) 
	`'(:package :url ,url :package ,url-package
	   :package-wethods ,(mapcar (compose #'unquote #'normalize-url-spec #'^wethod-spec)
				      wethods))))))




(defun check-function-discriminators (wethod-name discriminators)
  "If user is specifying the discriminators in the wethod-url-spec,
make sure that all such discriminators are associated with methods."
  (let ((valid-discriminators (get-function-discriminators wethod-name)))
    (aif (find-if (fn (discriminator) (not (find discriminator valid-discriminators :test #'equal)))
		  discriminators)
	 (error 'wisp-undefined-wethod-discriminator
		:discriminator it
		:valid-discriminators valid-discriminators
		:wethod-name wethod-name)
	 discriminators)))

(defun get-function-discriminators (function-name)
  "Get the discriminators of a function and sort them appropriately to
be used for urlmapping."
  (let ((discriminators
	 (mapcar (fn (method)
		   (mapcar (compose #'^symbol #'class-name)
			   (method-specializers method)))
		 (generic-function-methods (symbol-function
					    function-name)))))
    (sort-function-discriminators discriminators))) 

(defun sort-function-discriminators (discriminators)
  " All we care about is that the catch-all class `t' doesn't shadow
other classes. The actual class hierachy isn't important in
urlmapping." 
  (stable-sort discriminators
	       (fn (lst1 lst2)
		 (loop
		    for e1 in lst1
		    for e2 in lst2 
		    when (and (eq e1 t) (not (eq e2 t))) do (return nil)
		    when (and (eq e2 t) (not (eq e1 t))) do (return t)))))







(defun unparse-urlmap (urlmap)
  (with-slots (handlers wethods class-wethods packages nested-maps) urlmap
    (mapcar #'unparse-urlspec
	    (append handlers class-wethods wethods packages
		    nested-maps)))) 

(defgeneric unparse-urlspec ((url-spec url-spec)))

(defmethod unparse-urlspec ((url-spec url-spec))
  (with-slots (url url-regex) url-spec
    (list url url-regex)))

(defmethod unparse-urlspec ((url-spec url-handler))
  (with-slots (url url-regex dispatch) url-spec
    (list :handler (list :url url url-regex) :dispatch dispatch)))

(defmethod unparse-urlspec ((url-spec url-wethod))
  (with-slots (url url-regex dispatch discriminators wethod-urls wethod-url-regexes) url-spec
    (list :wethod (list :url url url-regex)
	  :dispatch dispatch
	  :discriminators discriminators
	  :wethod-urls (mapcar #'list wethod-urls wethod-url-regexes))))

(defmethod unparse-urlspec ((url-spec url-class-wethod))
  (with-slots (url url-regex dispatch discriminators wethod-urls wethod-url-regexes) url-spec
    (list :class-wethod (list :url url url-regex)
	  :dispatch dispatch
	  :discriminators discriminators 
	  :wethod-urls (mapcar #'list wethod-urls wethod-url-regexes))))

(defmethod unparse-urlspec ((url-spec url-nested-map))
  (with-slots (url url-regex nested-map-name) url-spec
    (list* :map (list :url url url-regex)
	   :nested-map (get-effective-urls nested-map-name))))

(defmethod unparse-urlspec ((url-spec url-package))
  (with-slots (url url-regex package urlmap) url-spec
    (list :package (list :url url url-regex) :package package
	  (cons :package-wethods
		(unparse-urlmap urlmap)))))




;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
