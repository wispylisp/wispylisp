;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


;; It might be interesting to use Cell for its constraint passing programming style.


(in-package :wisp-mvc)


(defpclass wisp-object () ()) 

(defun ^class-name (class-thing) 
  (etypecase class-thing
    ;; Not all classes are under standard class
    (standard-class (class-name class-thing))
    (symbol class-thing)
    (string (intern (string-invert class-thing)))))

(defun ^class (class-thing) 
  (find-class (^class-name class-thing)))


(defun find-class-btree (class) 
  (let* ((class-name (^class-name class)))
    (get-from-root class-name)))

(defun ensure-class-btree (class) 
  (aif (find-class-btree class)
       it
       (add-to-root class-name (make-btree))))

(defun find-instances (class)
  "Find all instances that belong to the class"
  (let* ((class-name (^class-name class))
	 (class-btree (get-from-root class-name))
	 (instances nil))
    (when class-btree (map-btree (fn (k v) (push v instances)) class-btree))
    instances))

(defun find-instances* (class)
  "Find all instances that belong to the class and its subclasses" 
  (let* ((class-name (^class-name class))
	 (sub-classes (mop:class-direct-subclasses (find-class class-name)))) 
    (append (find-instances class)
	    (mappend (fn (sub-class) (find-instances* sub-class))
		     sub-classes))))

;;;; Simple CRUD
;; ;; ;; Elephant takes care of update.

(defmethod create ((object wisp-object) &key force?)
  (let* ((class-btree (ensure-class-btree (class-of object)))
	 (stored-object (get-value (^string (ele::oid object)) class-btree))) 
    (if (and stored-object (not force?))
	(error "Object ~S of class ~S already created" object (class-of object))
	(setf (get-value (^string (ele::oid object)) class-btree) object))))

(defmethod retrieve (class oid)
  (awhen (find-class-btree (^symbol class)) (get-value oid it)))

(defmethod destroy ((object wisp-object) &key force?)
  (let ((class-btree (find-class-btree (class-of object))))
    (remove-kv (ele::oid object) class-btree)))


(defun slot-names (class) 
  (mapcar (fn (slot-def) (mop:slot-definition-name slot-def))
	  ;; class-slots instead of direct slots gives too much information in many cases.
	  (mop:class-slots (^class class))))

(defun slot-values (class object) 
  (mapcar (fn (slot-name)
	    ;; TODO: heed the caveat from elephant manual that slot-value might not work properly
	    (slot-value object slot-name))
	  (slot-names class)))

(defmethod wisp-object->sexp ((object wisp-object))
  (let ((class (class-of object)))
    (list (class-name class)
	  (cons (slot-names class)
		(slot-values class object)))))

(defgeneric print-object-content (object))

(defmethod print-object-content (object))

(defmethod print-object-content ((object wisp-object))
  (print (wisp-object->sexp object)))


