(in-package :wisp-mvc)

(deftag :layout-div (&key child-priority &other-keys others &rest body)
  (flet ((make-pane (child)
	   (let ((align (car child)))
	     (if (find align '(:top :bottom :left :right :client :flood))
		 `(:pane :layout-align ,(^string align) ,@(cdr child))
		 child))))
  `(:div :dojoType "LayoutContainer"
	 ,@(when child-priority `(:layout-child-priority ,child-priority))
	 ,@others
	 ,@(mapcar #'make-pane body))))

#+(or)
(:body
     (:layout-div :child-priority "none" :style (:width 80% :height 300px)
      (:top :style (:background red) "top")
      (:bottom :style (:background $000000 :color $ffffff) "bottom")
      (:left :style (:background $444444 :color $ffffff) "left")
      (:right :style (:background $888888)"right")
      (:top :style (:background $cccccc)"top 2")
      (:right :style (:background green :color $ffffff) "right")
      (:bottom :style (:background blue)"bottom 2")
      (:left :style (:background yellow :color black) "left")
      (:client :style (:text-align center)"How about 42?")))


(deftag :content-pane (&rest body) 
  `(:div :dojoType "ContentPane" 
	 ,@body))

(deftag :pane (&rest body) 
  `(:content-pane ,@body))



(deftag :tab-div (&key selected-tab ;; the id of the selected tab. (Or use the 'selected' in a tab)
			     close-button ;; "pane" or "tab"
			     label-position ;; "top", "left-h", "right-h", "bottom"
			     style (dimension '(:width 100% :height 100%))
			     &other-keys others &rest body)
  ;; a tab within the container may set the "onClose" callback, the "selected" property, and the refreshOnShow property.
  `(:div :dojoType "TabContainer"
	 :style ,(append dimension style) ;; style takes precedence over dimension.
	 ,@(when selected-tab `(:selected-tab ,selected-tab))
	 ,@(when close-button `(:close-button ,close-button))
	 ,@(when label-position `(:label-position ,label-position)) 
	 ,@others
	 ,@body))

#+(or)
(:tab-div :layout-align 'top
		(:pane :label "Tab 1" "0")
		(:pane :href "/src/silly.txt" :label "Tab 2" "a")
		(:pane :refresh-on-show 'false :label "Tab 3" "b")
		(:tab-div :label "Nested Tab"
				(:pane :label "Tab 3.1" "hi")
				(:pane :label "Tab 3.2" "hello")))

(deftag :accordion-div (&key label-class ;; for attaching style-sheet to the accordion label
			     div-class ;; for attaching style-sheet to the div container
			     &other-keys others
			     &rest body)
  `(:div :dojoType "AccordionContainer"
	 
	 ,@(when label-class `(:label-node-class ,label-class))
	 ,@(when div-class `(:container-node-class ,div-class))
	 ,@others
	 ,@body))

#+(or)
(:body
 (:css (.accBody :background white)
       (.accLabel :background black :color white :border 1px solid white))
 (:accordion-div :div-class 'acc-body :label-class 'acc-label
		 :style (:height 400px :width 300px :border 1px solid black)
		 (:pane :label "sect 1"  :open 'true
			(:div :style (:height 110px) "moohahaha"))
		 (:pane :label "sect 2" (:div "bwahahaha"))
		 (:pane :label "sect 3" (:div "mahahahahaha"))))


(deftag :split-div (orientation &key (sizer-width 10) (active-sizing 0) &other-keys others &rest body) 
  `(:div :dojoType "SplitContainer"
	 :orientation ,orientation :sizerWidth ,sizer-width :activeSizing ,active-sizing 
	 ,@others
	 ,@body))

(deftag :split-v (&other-keys others &body body)
  `(:split-div "vertical" ,@others ,@body))

(deftag :split-h (&other-keys others &body body)
  `(:split-div "horizontal" ,@others ,@body))

#+(or)
(:split-v :style (:width 400px :height 400px :border 1px solid black) 
	  (:content-pane "Hello1")
	  (:split-h 
	   (:content-pane "Hello2")
	   (:content-pane "Hello3")))



(deftag :tree (&key (toggle "fade") (size-min 20) (size-share 20) &rest trees) 
  `(:div :dojoType "Tree" :sizeMin ,size-min :sizeShare ,size-share :toggle ,toggle
	 ,@(mapcar #'generate-subtree trees)))

(defun generate-subtree (tree)
  (if (consp tree)
      `(:div :dojoType "TreeNode" :title ,(car tree)
	     ,@(mapcar (fn (kid) (generate-subtree kid))
		      (cdr tree)))
      `(:div :dojoType "TreeNode" :title ,tree)))

#+(or)
(:tree
 ("Item1" "Item 1.1"
	  ("Item 1.2" "Item 1.2.1" "Item 1.2.2")
	  ("Item 1.3" "Item 1.3.1" "Item 1.3.2")
	  ("Item 1.4" "Item 1.4.1" "Item 1.4.2")))



(deftag :toggler (target body)
  ;; can't get it to work.
  (cons (car body) (append `(:dojoType "Toggler" :targetId ,target) (cdr body))))

;; (:div "hello" (:br)
;;       (:toggler "cruel"
;; 		(:button "click to say good bye."))
;;       (:div :id "cruel" :toggle "fade" "cruel world")) 


