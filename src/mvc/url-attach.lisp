(in-package :wisp-mvc)


(defun attach-urlmap (url-prefix map-name)
  "Registers a urlmap under the provided url-prefix."
  (let ((map (get-urlmap map-name))
	(url-prefix (ensure-prefix-slash-only url-prefix)))
    (detect-circular-map-nesting map)
    (with-slots (url-prefixes) map
      (setf url-prefixes (adjoin url-prefix url-prefixes)))
    (publish-prefix :prefix url-prefix
		    :content-type "text/html"
		    :function (^url-dispatcher url-prefix map))))

(defun detect-circular-map-nesting (urlmap)
  "Detects circular and undefined url mappings."
  (let ((*all-maps* (make-hash-table)))
    (labels ((rec-test (urlmap2)
	       (if urlmap2
		   (dolist (map-name (get-nest-map-names urlmap2))
		     (let ((nested-urlmap (get-urlmap map-name)))
		       (cond ((null nested-urlmap)
			      (error 'wisp-undefined-nested-urlmap
				     :nested-map map-name
				     :root-map urlmap))
			     ((gethash nested-urlmap *all-maps*)
			      (error 'wisp-circular-nested-urlmap
				     :circular-map nested-urlmap
				     :parent-map urlmap2
				     :root-map urlmap))
			     (t (setf (gethash nested-urlmap *all-maps*) t)
				;; recursively test for cicularity
				(dolist (map-name (get-nest-map-names nested-urlmap))
				  (aif (get-urlmap map-name)
				       (rec-test it)
				       (error 'wisp-undefined-nested-urlmap
					      :nested-map map-name
					      :root-map nested-urlmap))))))))))
      (setf (gethash urlmap *all-maps*) t)
      (rec-test urlmap))))

(defun get-nest-map-names (urlmap)
  (mapcar #'url-nested-map-name
	  (slot-value urlmap 'nested-maps)))
    


(defun detach-urlmap (map-name)
  ;; Don't know how to remove a published handler
  (dolist (url (urls (get-urlmap map-name)))
    (publish-prefix :prefix url
		    :function nil)))




