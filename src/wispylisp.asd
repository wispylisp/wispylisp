
(defpackage wispy-lisp-system
  (:use :cl :asdf))

(in-package :wispy-lisp-system)


(defsystem :wispylisp
  :description "Wispy Lisp: a web-framework."
  :version "0.001"
  :author "Howard Yeh <hayeah at gmail dot com>"
  :licence "BSD"
  :components ((:file "packages")
	       (:file "wisp-sys")
	       (:module :util :components
			((:file "util")
			 (:file "rkr-macro")
			 (:file "fmt")
			 (:file "condition"))
			 :serial t
			 :depends-on ("packages"))
	       (:module :patch :components
			((:file "namespace")
			 (:module :arnesi :components
				  ((:file "walk")
				   (:file "generic-walker")
				   (:file "no-call-cc" :depends-on ("generic-walker")))
				  :depends-on ("namespace"))
			 (:module :foo :components
				  ((:file "foo-packages")
				   (:file "text-output")
				   (:file "language")
				   (:file "css")
				   (:file "javascript")
				   (:file "html")
				   (:file "deftag"))))
			:depends-on (:util))
	       (:module :mvc :components
			((:file "model")
			 (:file "view")
			 (:file "dojo-widget")
			 (:file "dojo-js")
			 (:file "control")
			 (:file "form-def" :depends-on ("view"))
			 (:file "form-render" :depends-on ("view"))
			 (:file "url-map")
			 (:file "url-dispatch" :depends-on ("url-map"))
			 (:file "url-attach" :depends-on ("url-dispatch"))
			 (:file "wisp-tags"))
			:depends-on ("packages" :util :patch)))
  :depends-on (:com.gigamonkeys.foo
	       :elephant :ele-bdb
;;	       :net.aserve
	       :cl-interpol :cl-ppcre
	       :arnesi :fiveam
	       :kmrcl
	       :unification
	       ))


;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
