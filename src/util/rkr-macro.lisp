
(in-package :wisp-util)
(eval-when (:compile-toplevel :load-toplevel :execute)


(defun parse/rkr/fnlist (args) 
  (destructuring-bind (required keys other-keys body)
      (split-by-separators args `(&key &other-keys
				       ,(fn (arg) (find arg '(&body &rest)))))
    (list required
	  (mapcar #'mklst (cdr keys))
	  (cdr other-keys)
	  (cdr body)))) 


(defun parse/rkr/args (fn-type fn-name arg-list required-args valid-keys &key (other-keys? nil) (rest-args? nil))
  "Returns (list required keys other-keys rest)" 
  (flet ((valid-key? (key) (find key valid-keys)) 
	 (length<2 (list)
	   (loop with i = 0
		for e in list do (incf i)
		when (>= i 2) do (return nil)
		finally (return t))))
    (let* ((args-length (length required-args))
	   (required (subseq arg-list 0 args-length))
	   (keys/rest-args (subseq arg-list args-length)))
      (if (null keys/rest-args) (list required nil nil nil)
	  (loop for rest on keys/rest-args by #'cddr 
	     until (or (not (keywordp (car rest))) (length<2 rest))
	     for key = (first rest) for arg = (second rest) 
	     if (valid-key? key) append (list key arg) into keys
	     else if other-keys? append (list key arg) into other-keys
	     else do (error "Invalid key ~S for rkr lambda-list. ~S" key (append (list fn-type fn-name) arg-list)) end 
	     finally (return
		       (if (and rest (not rest-args?))
			   (error "Too many arguments for rkr lambda-list. ~S. Maybe you forgot &rest?"
				  (append (list fn-type fn-name) arg-list))
			   (list required keys other-keys rest))))))))

(defun parse/rkr/args-test (&rest args)
  (apply #'parse/rkr/args 'foo 'bar args))



;; (parse/rkr/args-test  '(:a 1 2 3) nil nil :rest-args? t) 
;; ;; error.  Is this the right behavior? 
;; ;; ;; yes. Because in all other cases the first of the rest args can't be a keyword.


;; (parse/rkr/args-test  '(1 2) '(a) nil :rest-args? t) 
;; ((1) nil nil (2))


;; (parse/rkr/args-test  '(1 2) '(a b) nil :rest-args? t) 
;; ((1 2) nil nil nil)


;; (parse/rkr/args-test  '(1 2) '(a b) nil) 
;; ((1 2) nil nil nil)

;; (parse/rkr/args-test  '(1 2 3 4) '(a b) nil) 
;; ;; error

;; (parse/rkr/args-test  '(:a 1 :b 2) nil '(:a :b) :rest-args? t) 
;; (nil (:a 1 :b 2) nil nil)


;; (parse/rkr/args-test  '(:a 1 :b 2) nil '(:a :b)) 
;; (nil (:a 1 :b 2) nil nil)

;; (parse/rkr/args-test  '(:a 1 :b 2 :c 3 :d 4) nil '(:a :b) :other-keys? t)
;; (nil (:a 1 :b 2) (:c 3 :d 4) nil)

;; (parse/rkr/args-test  '(:a 1 :b 2 :c 3 :d 4) nil '(:a :b) :rest-args? t)
;; ;; error


;; (parse/rkr/args-test  '(1 2 4 5 6 7) '(arg1 arg2) '(:a :b) :rest-args? t) 
;; ((1 2) nil nil (4 5 6 7))


;; (parse/rkr/args-test  '(1 2 :a 3 :b 4 5 6 7) '(arg1 arg2) '(:a :b) :rest-args? t) 
;; ((1 2) (:a 3 :b 4) nil (5 6 7))


;; (parse/rkr/args-test  '(1 2 :a 3 :b 4 :c 5 6 7) '(arg1 arg2) '(:a :b) :rest-args? t) 
;; ;; error

;; (parse/rkr/args-test  '(1 2 :a 3 :e 6 :b 4 :c 5 6 7) '(arg1 arg2) '(:a :b) :other-keys? t :rest-args? t) 
;; ((1 2) (:a 3 :b 4) (:e 6 :c 5) (6 7))





(defun build/rkr/fnlist (required-args key-args other-keys rest)
  `(,@required-args
    ,@(when (or rest other-keys key-args) '(&key)) ;; passes in rest and other-keys as key args.
    ,@rest
    ,@other-keys
    ,@key-args))


	
;; Someday support the extended lambda list. (Destructuring args)
(defmacro def/rkr/fn-type (fn-type name fnlist &body body)
  (let* ((fn (dbind (required-args key-args other-keys rest) (parse/rkr/fnlist fnlist) 
	      `(fn (&rest arg-list) 
		 (flet ((bind-args ,(build/rkr/fnlist required-args key-args other-keys rest) ,@body))
		   (dbind (required keys other-keys rest-args)
			  (parse/rkr/args
			   ,fn-type ',name
			   arg-list ',required-args ',(mapcar (compose #'^keyword  #'car)
							      key-args)
					  :other-keys? ,(consp other-keys)
					  :rest-args? ,(consp rest))
			   (let ((keys (append (when rest-args (list ,(^keyword (car rest)) rest-args))
					       (when other-keys (list ,(^keyword (car other-keys)) other-keys))
					       keys)))
			     ;; sneaks in rest and other-keys as if they are key args
			     (apply #'bind-args (append required keys))))))))
	 (fn (if (eql fn-type :defmacro)
		 ;; gets rid of macro-name for macro expansion, for now.
		 ;; ;; TODO deal with &environment and &whole
		 `(fn (&rest arg-list) (apply ,fn (cdar arg-list))) 
		 fn)))
    (ecase fn-type
      (:defmacro `(progn (-> (macro-function ',name) ,fn) ',name))
      (:defun `(progn (-> (symbol-function ',name) ,fn) ',name))
      (:fn `,fn))))



(defmacro def/rkr/macro (name fnlist &body body)
  `(def/rkr/fn-type :defmacro ,name ,fnlist ,@body))

(defmacro def/rkr/fun (name fnlist &body body)
  `(def/rkr/fn-type :defun ,name ,fnlist ,@body))

(defmacro rkr/fn (fnlist &body body)
  `(def/rkr/fn-type :fn nil ,fnlist ,@body))





;; (funcall (rkr/fn (a b &key c d &rest rest) (list a b c d rest)) 1 2 :d 3 4 5) 
;; (1 2 nil 3 (4 5)) 

;; (funcall (rkr/fn (a b &key c d &other-keys e &rest rest) (list a b c d e rest)) 1 2 :d 4 7 8) 
;; (1 2 nil 4 nil (7 8)) 

;; (funcall (rkr/fn (a b &key c d &other-keys e) (list a b c d e)) 1 2 :d 4 :e 7)



;; (funcall (rkr/fn (a b &key c d &other-keys e &rest rest) (list a b c d e rest)) 1 2 :d 4 :e 5 :f 6 7 8) 
;; (1 2 nil 4 (:e 5 :f 6) (7 8))
;; 

;; (def/rkr/fun foo (a b &key c d &rest rest) (list a b c d rest)) 
;; (foo 1 2 :d 3 4 5)


(defun push/rkr/keyarg (keyarg fnlist)
  (if fnlist
      (loop for (arg . args) = fnlist then args
	    until (or (eql arg '&rest) (eql arg '&key))
	    collect arg into req
	    while args
	    finally (return (if (eql arg '&rest)
				(append req (list '&key keyarg) (cons '&rest args))
				(append req (append (list '&key  keyarg) args)))))
      (list '&key keyarg)))


) ;; eval-when
