

(in-package :wisp-util)


(defun mac (body n)
  (do ((body body (macroexpand-1 body))
       (n n (1- n)))
      ((zerop n) body)))

(defun maptree (fn x) 
  (if (atom x) 
      (funcall fn x) 
      (let ((a (funcall fn (car x))) 
            (d (maptree fn (cdr x)))) 
        (if (and (eql a (car x)) (eql d (cdr x))) 
            x 
            (cons a d)))))



(defun try-bq (form &optional (n 0))
  (do ((form (macroexpand form) (eval form))
       (str " = " "~% => ")
       (j 0 (+ j 1)))
      ((>= j n)
       (format t str)
       (write form :pretty t))
    (format t str)
    (write form :pretty t)))

(defun split-by-separators (lst test-lst &key ((:test default-test) #'eql))
  "The provided list of tests are used as separators to break a list into list of alists.
Each alist is headed by the separator.
The if the test if an not a procedure, it is compared to each element of the list with eql as default.
-
The list to be splitted must satisfy 2 conditions:
1) The separators must occur in sequential order.
2) Separators must not occur twice in the same list.
-
Some separators may be omitted from the list, in which case, nil account for them in the resulting list.
-
Return:
 (<args-before-first-separator> [[<separator> <args-between-this-and-next-separator>) | nil]]*) "
  (unless lst (return-from split-by-separators (make-list (1+ (length test-lst)))))
  (flet ((do-test (test obj) 
	   (if (functionp test)
	       (funcall test obj)
	       (funcall default-test test obj)))) 
  (loop with todo-tests = test-lst
	with done-tests = nil
        with section = nil		; What I really want is use collect instead, but see next comment
	for objs on lst
	for obj = (car objs)
        for (attempted-tests separator separator?) =
            (loop for test in todo-tests
	       collect test into attempted-tests
	       thereis (and (do-test test obj)
			    (setq done-tests (append attempted-tests done-tests))
			    (list attempted-tests obj t))
	       finally (list attempted-tests nil nil))
        if separator? 
	  nconc (append (list (nreverse section))
			(make-list (1- (length attempted-tests)))) into sections 
	  and do (setq section (list obj) ; I need to set section nil here, but if using collect would reset it back.
		       todo-tests (cdr (member separator todo-tests)))
          and do (unless (cdr objs) (return (nconc sections (list section) (make-list (length todo-tests)))))
	else do
        (cond ((loop for test in done-tests thereis (do-test test obj))
	       (error "repeated separator ~S found. With tests ~S on list ~S" obj test-lst lst))
	      ;; append unfinished tests and attempted tests
	      ;; ;; actually, at this point, all unfinished tests are found in attempted tests
	      ((null (cdr objs)) (return (progn (nconc sections
						       (list (nreverse (push obj section)))
						       (make-list (length todo-tests))))))
	      (t (push obj section)))
     end)))


(defun ^string (o)
  (typecase o
    (symbol (string-invert (symbol-name o)))
    (string o)
    (t (format nil "~S" o))))

(defun ^symbol (symbol-thing)
  (etypecase symbol-thing
    (symbol symbol-thing)
    (string (intern (string-invert symbol-thing)))))

(defun ^keyword (symbol-thing)
  (etypecase symbol-thing
    (symbol (intern (symbol-name symbol-thing) :keyword))
    (string (intern (string-invert symbol-thing) :keyword))
    (package (^keyword (intern (package-name symbol-thing))))))

(defun ^package (symbol-thing)
  (etypecase symbol-thing
    (symbol (find-package (^keyword symbol-thing)))
    (string (find-package string))))

(defun unquote (form)
  ;; ugly hack sometimes useful for defining macros.
  ;; Short of using eval...
  (if (and (consp form) (eql (car form) 'quote))
      (unquote (second form))
      form)) 


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun group (source n)
    (if (zerop n) (error "zero length"))
    (labels ((rec (source acc)
	       (let ((rest (nthcdr n source)))
		 (if (consp rest)
		     (rec rest (cons (subseq source 0 n) acc))
		     (nreverse (cons source acc))))))
      (if source (rec source nil) nil)))
  )

(defmacro abbrev (short long)
  `(defmacro ,short (&rest args) `(,',long ,@args)))

(defmacro abbrevs (&rest names)
  `(progn ,@(mapcar #'(lambda (pair) `(abbrev ,@pair))
		    (group names 2))))

(abbrevs mvbind multiple-value-bind
	 mvsetq multiple-value-setq
	 dbind destructuring-bind
	 -> setf
	 --> set
	 
;; 	 <- getf
;; 	 <-- get
	 )

(defmacro fn (args &body body)
  (cond
   ((null args) `#'(lambda () ,@body))
   ((atom args) `#'(lambda (&rest ,args) ,@body))
   ((cdr (last args))
     (let* ((args (copy-list args))
             (last (last args)))
       (setf (cdr last) (list '&rest (cdr last)))
       `#'(lambda ,args ,@body)))
   (t `#'(lambda ,args ,@body))))


(defun mklst (obj)
  (if (listp obj) obj (list obj)))

(defun cat-syms (&rest los)
  (values (intern (apply #'concatenate (cons 'string (mapcar #'symbol-name los))))))

;; (defun strcat (&rest strings)
;;   (apply 'concatenate 'string strings))

(defmacro with-char-collector (var &rest body)
  (let ( (svar (gensym "S")) )
    `(let ( (,svar (make-array 0 :element-type 'base-character
                               :fill-pointer t :adjustable t)) )
       (flet ( (,var (c) (vector-push-extend c ,svar)) )
         ,@body
         ,svar))))

(defun symcat (&rest symbols)
  (intern (string-invert (apply 'strcat (mapcar #'^string symbols)))))




;; (defun pp (&rest args)
;;   (pprint args) 
;;   (values))

(defun intersection-in-order (lst1 lst2 &key (key #'identity) (test #'eql))
  "Get the intersection of two sets, with elements in the order of the first list given as argument."
  (let ((hash (make-hash-table :test test))
	result)
    (dolist (e lst2) 
      (push e (gethash (funcall key e) hash)))
    (dolist (e lst1) 
      (push e (gethash (funcall key e) hash)))
    (print hash)
    (dolist (k lst1) 
      (awhen2 (gethash (funcall key k) hash)
	(when (> (length it) 1)
	  (push (car it) result))))
    (nreverse result)))

(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
	(if val (push val acc))))
    (nreverse acc)))

(defun mappend (fn &rest lsts)
  "Non-destructive mapcan"
  (apply #'append (apply #'mapcar fn lsts)))



;; doesn't support fancy lambda list with &optional, &rest, etc.

;; (defmacro dbind (pat seq &body body)
;;   "Destructuring bind for type sequence. (Ch. 19 Onlisp)"
;;   (let ((gseq (gensym)))
;;     `(let ((,gseq ,seq))
;;       ,(dbind-ex (destruc pat gseq #'atom) body))))

;; (defun destruc (pat seq &optional (atom? #'atom) (n 0))
;;   (if (null pat)
;;       nil
;;       (let ((rest (cond ((funcall atom? pat) pat)
;; 			((eq (car pat) '&rest) (cadr pat))
;; 			((eq (car pat) '&body) (cadr pat))
;; 			(t nil))))
;; 	(if rest
;; 	    `((,rest (subseq ,seq ,n)))
;; 	    (let ((p (car pat))
;; 		  (rec (destruc (cdr pat) seq atom? (1+ n))))
;; 	      (if (funcall atom? p)
;; 		  (cons `(,p (elt ,seq ,n))
;; 			rec)
;; 		  (let ((var (gensym)))
;; 		    (cons (cons `(,var (elt ,seq ,n))
;; 				(destruc p var atom?))
;; 			  rec))))))))

;; (defun dbind-ex (binds body)
;;   (if (null binds)
;;       `(progn ,@body)
;;       `(let ,(mapcar #'(lambda (b)
;; 			 (if (consp (car b))
;; 			     (car b)
;; 			     b))
;; 		     binds)
;; 	,(dbind-ex (mapcan #'(lambda (b)
;; 			       (if (consp (car b))
;; 				   (cdr b)))
;; 			   binds)
;; 		   body))))

;; (defmacro dlet (binds &body body)
;;   (cond ((null binds) `(progn ,@body))
;; 	((null (cdr binds)) (error (format nil "dlet syntax error: unmatched binding pair in:~&~s" binds)))
;; 	((consp (car binds)) `(dbind ,(car binds) ,(cadr binds) (dlet ,(cddr binds)  ,@body)))
;; 	(t `(let ((,(car binds) ,(cadr binds))) (dlet ,(cddr binds) ,@body)))))
	



;; Anaphoric Macros from kmrcl

(defmacro afn (parms &body body)
  `(labels ((self ,parms ,@body))
     #'self)) ;; funny


(defmacro aif2 (test &optional then else)
  (let ((win (gensym)))
    `(multiple-value-bind (it ,win) ,test
       (if (or it ,win) ,then ,else))))

(defmacro awhen2 (test &body body)
  `(aif2 ,test
         (progn ,@body)))

(defmacro awhile2 (test &body body)
  (let ((flag (gensym)))
    `(let ((,flag t))
       (while ,flag
         (aif2 ,test
               (progn ,@body)
               (setq ,flag nil))))))

#+(or)
(defmacro acond2 (&rest clauses)
  (if (null clauses)
      nil
      (let ((cl1 (car clauses))
            (val (gensym))
            (win (gensym)))
        `(multiple-value-bind (,val ,win) ,(car cl1)
           (if (or ,val ,win)
               (let ((it ,val)) ,@(cdr cl1))
               (acond2 ,@(cdr clauses)))))))

(defmacro aprogn (&rest forms)
  (cond  ((null forms) nil)
	 ((null (cdr forms)) (car forms))
	 (t
	  `(let ((it ,(car forms)))
	     (aprogn ,@(cdr forms))))))






(proclaim '(inline single conc1 mklist))

;; (defun last1 (lst)
;;   (car (last lst)))

(defun single (lst)
  (and (consp lst) (not (cdr lst))))

;; (defun append1 (lst obj)
;;   (append lst (list obj)))

(defun conc1 (lst obj)
  (nconc lst (list obj)))

(defun mklist (obj)
  (if (listp obj) obj (list obj)))

(defun longer (x y)
  (labels ((compare (x y)
	     (and (consp x)
		  (or (null y)
		      (compare (cdr x) (cdr y))))))
    (if (and (listp x) (listp y))
	(compare x y)
	(> (length x) (length y)))))





(defun flatten (x)
  (labels ((rec (x acc)
	     (cond ((null x) acc)
		   ((atom x) (cons x acc))
		   (t (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))

(defun prune (test tree)
  (labels ((rec (tree acc)
	     (cond ((null tree) (nreverse acc))
		   ((consp (car tree))
		    (rec (cdr tree)
			 (cons (rec (car tree) nil) acc)))
		   (t (rec (cdr tree)
			   (if (funcall test (car tree))
			       acc
			       (cons (car tree) acc)))))))
    (rec tree nil)))

					; return both the item and the test-result.

(defun find2 (fn lst)
  (if (null lst)
      nil
      (let ((val (funcall fn (car lst))))
	(if val
	    (values (car lst) val)
	    (find2 fn (cdr lst))))))
					; 
;; 
(defun before (x y lst &key (test #'eql))
  "Test if x occurs before y in lst. Returns (x ...) if y doesn't occur at all (i.e. equivalent to #'member's result)."
  (and lst
       (let ((first (car lst)))
	 (cond ((funcall test y first) nil)
	       ((funcall test x first) lst)
	       (t (before x y (cdr lst) :test test))))))


(defun after (x y lst &key (test #'eql))
  (let ((rest (before y x lst :test test)))
    (and rest (member x rest :test test))))

(defun duplicate (obj lst &key (test #'eql))
  (member obj (cdr (member obj lst :test test))
	  :test test))

(defun split-if (fn lst)
  (let ((acc nil))
    (do ((src lst (cdr src)))
	((or (null src) (funcall fn (car src)))
	 (values (nreverse acc) src))
      (push (car src) acc))))

(defun most (fn lst)
  "returns the highest scored element in lst calculated with fn"
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
	     (max (funcall fn wins)))
	(dolist (obj (cdr lst))
	  (let ((score (funcall fn obj)))
	    (when (> score max)
	      (setq wins obj
		    max score))))
	(values wins max))))


(defun best (fn lst)
  "Returns the winner by comparing all elements with the 2-place predicate. Like #'car of #'sort"
  (if (null lst)
      nil
      (let ((wins (car lst)))
	(dolist (obj (cdr lst))
	  (if (funcall fn obj wins)
	      (setq wins obj)))
	wins)))


(defun mostn (fn lst)
  "like most, except it collects all the element with the same highscore"
  (if (null lst)
      (values nil nil)
      (let ((result (list (car lst)))
	    (max (funcall fn (car lst))))
	(dolist (obj (cdr lst))
	  (let ((score (funcall fn obj)))
	    (cond ((> score max)
		   (setq max score
			 result (list obj)))
		  ((= score max)
		   (push obj result)))))
	(values (nreverse result) max))))


(defun map0-n (fn n) 
  "map over a generated list of (0 ... n) inclusive"
  (mapa-b fn 0 n))

(defun map1-n (fn n)
  "map over a generated list of (1 ... n) inclusive"
  (mapa-b fn 1 n))

(defun mapa-b (fn a b &optional (step 1))
  "map over generated list of (a ... b) inclusive"
  (do ((i a (+ i step))
       (result nil))
      ((> i b) (nreverse result))
    (push (funcall fn i) result)))

(defun map-> (fn start test-fn succ-fn)
  "Map over a generated list"
  (do ((i start (funcall succ-fn i))
       (result nil))
      ((funcall test-fn i) (nreverse result))
    (push (funcall fn i) result)))



;; mapcan may be thought of as the above, but applying #'nconc

(defun mapcars (fn &rest lsts)
  (let ((result nil))
    (dolist (lst lsts)
      (dolist (obj lst)
	(push (funcall fn obj) result)))
    (nreverse result)))

(defun rmapcar (fn &rest args)
  "recursive mapcar"
  (if (some #'atom args)
      (apply fn args)
      (apply #'mapcar
	     #'(lambda (&rest args)
		 (apply #'rmapcar fn args))
	     args)))

(defmacro when-bind* (binds &body body)
  (if (null binds)
      `(progn ,@body)
      `(let (,(car binds))
	(if ,(caar binds)
	    (when-bind* ,(cdr binds) ,@body)))))

(defmacro condlet (clauses &body body)
  (let ((bodfn (gensym))
        (vars (mapcar #'(lambda (v) (cons v (gensym)))
                      (remove-duplicates
                       (mapcar #'car
                               (mappend #'cdr clauses))))))
    `(labels ((,bodfn ,(mapcar #'car vars)
                      ,@body))
       (cond ,@(mapcar #'(lambda (cl)
                            (condlet-clause vars cl bodfn))
                        clauses)))))

(defun condlet-clause (vars cl bodfn)
  `(,(car cl) (let ,(mapcar #'cdr vars)
                (let ,(condlet-binds vars cl)
                  (,bodfn ,@(mapcar #'cdr vars))))))

(defun condlet-binds (vars cl)
  (mapcar #'(lambda (bindform)
              (if (consp bindform)
                  (cons (cdr (assoc (car bindform) vars))
                        (cdr bindform))))
          (cdr cl)))

(defmacro condlet* (clauses &body body)
  (let ((bodfn (gensym))
	(vars (mapcar #'(lambda (v) (cons v (gensym)))
		      (remove-duplicates
		       (mapcar #'car
			       (mappend #'cdr clauses))))))
    `(labels ((,bodfn ,(mapcar #'car vars)
	       ,@body))
      (cond ,@(mapcar #'(lambda (cl)
			  (condlet*-clause vars cl bodfn))
		      clauses)))))

(defun condlet*-clause (vars cl bodfn)
  `(,(car cl) (let* ,(mapcar #'cdr vars)
		(let* ,(condlet*-binds vars cl)
		  (,bodfn ,@(mapcar #'cdr vars))))))

(defun condlet*-binds (vars cl)
  (mapcar #'(lambda (bindform)
	      (if (consp bindform)
		  (cons (cdr (assoc (car bindform) vars))
			(cdr bindform))))
	  (cdr cl)))

;; I don't get this
;; (defmacro if3 (test t-case nil-case ?-case)
;;   `(case ,test
;;      ((nil) ,nil-case)
;;      (? ,?-case)
;;      (t ,t-case)))


(defmacro nif (expr pos zero neg)
  "numeric (+,0,-) 3-ways if"
  (let ((g (gensym)))
    `(let ((,g ,expr))
      (cond ((plusp ,g) ,pos)
	    ((zerop ,g) ,zero)
	    (t ,neg)))))

;; More efficient way to test set membership, when elements are not in a list yet.
;; (in x 2 3 4) == (member x (list 2 3 4))

(defmacro in (obj &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,obj))
      (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
		    choices)))))

;; with the set elements quoted
;; (inq x + - $) == (in x '+ '- '$)
(defmacro inq (obj &rest args)
  `(in ,obj ,@(mapcar #'(lambda (a)
			  `',a)
		      args)))

;; (in-if #'(lambda (y) (equal x y)) a b) == (member x (list a b) :test #'equal)
;; (in-if #'oddp a b) == (some #'oddp a b)
(defmacro in-if (fn &rest choices)
  (let ((fnsym (gensym)))
    `(let ((,fnsym ,fn))
      (or ,@(mapcar #'(lambda (c)
			`(funcall ,fnsym ,c))
		    choices)))))

;; I can't decide how much these "in"s improves overall efficiency.
;; I mean, how often would I have 1 thousand variables not in list, and want to test membership?
;; In normal circumstances, it seems that the gain is fairly marginal. But then, it adds up.
;;;; so if i have on my hand a list, then it doesn't make sense to use these macros.


;; Like case, but can have expression as keys. Keys must be in list, except 't' and 'otherwise'
(defmacro >case (expr &rest clauses)
  (let ((g (gensym)))
    `(let ((,g ,expr))
      (cond ,@(mapcar #'(lambda (cl) (>casex g cl))
		      clauses)))))

(defun >casex (g cl)
  (let ((key (car cl)) (rest (cdr cl)))
    (cond ((consp key) `((in ,g ,@key) ,@rest))
	  ((inq key t otherwise) `(t ,@rest))
	  (t (error "bad >case clause")))))

;; (>case 7
;;        ((1 (+ 3 4)) 1)
;;        ((3) 2)
;;        (((+ 1 1) 4) 3)
;;        ((nil) 5)
;;        (t 4))



;; ;;;; This is excellent!
;; (defmacro do-tuples/o (parms source &body body)
;;   (if parms
;;       (let ((src (gensym)))
;; 	`(prog ((,src ,source))
;; 	       (mapc #'(lambda ,parms ,@body)
;; 		     ,@(map0-n #'(lambda (n)
;; 				    `(nthcdr ,n ,src))
;; 				(1- (length parms))))))))

;; (do-tuples/o (x y z) '(1 2 3 4)
;; 	     (princ (list x y z)))


;; Alternate definition with loop macro
(defmacro do-tuples/o (parms source &body body)
  (if parms
      (let ((src (gensym)))
	`(prog ((,src ,source))
	  (mapc #'(lambda ,parms ,@body)
	   ,@(loop for n from 0 to (1- (length parms))
		   collect `(nthcdr ,n ,src)))))))

(defmacro do-tuples/c (parms source &body body)
  (if parms
      (with-gensyms (src rest bodfn)
	(let ((len (length parms)))
	  `(let ((,src ,source))
	    (when (nthcdr ,(1- len) ,src)
	      (labels ((,bodfn ,parms ,@body))
		(loop for ,rest on ,src
		      while (nthcdr ,(1- len) ,rest)
		      do (,bodfn ,@(loop for ri to (1- len)
					 collect `(nth ,ri ,rest)))
		      finally (progn
				,@(loop for ri to (- len 2)
					collect `(,bodfn
						  ,@(loop for i from ri to (- len 2)
							  collect `(nth ,i ,rest))
						  ,@(loop for si from 0 to ri
							  collect `(nth ,si ,src))))
				nil)))))))))


;;;; This is tight!!!! Holy moly.
(defmacro do-tuples/c2 (parms source &body body)
  (if parms
      (with-gensyms (src rest bodfn)
	(let ((len (length parms)))
	  `(let ((,src ,source))
	    (when (nthcdr ,(1- len) ,src)
	      (labels ((,bodfn ,parms ,@body))
		(do ((,rest ,src (cdr ,rest)))
		    ((not (nthcdr ,(1- len) ,rest))
		     ,@(mapcar #'(lambda (args)
				   `(,bodfn ,@args))
			       (dt-args len rest src))
		     nil)
		  (,bodfn ,@(map1-n #'(lambda (n)
					`(nth ,(1- n)
					  ,rest))
				    len))))))))))

;; Doesn't look very efficient if tuples are very long.
;; But tuples generally are short. So...
(defun dt-args (len rest src)
  (map0-n #'(lambda (m)
	      (map1-n #'(lambda (n)
			  (let ((x (+ m n)))
			    (if (>= x len)
				`(nth ,(- x len) ,src)
				`(nth ,(1- x) ,rest))))
		      len))
	  (- len 2)))



(defmacro mvdo (binds (test &rest result) &body body)
  (let ((label (gensym))
	(temps (mapcar #'(lambda (b)
			   (if (listp (car b))
			       (mapcar #'(lambda (x)
					   (gensym))
				       (car b))
			       (gensym)))
		       binds)))
    `(let ,(mappend #'mklist temps)
      (mvpsetq ,@(mapcan #'(lambda (b var)
			     (list var (cadr b)))
			 binds
			 temps))
      (prog ,(mapcar #'(lambda (b var) (list b var))
		     (mappend #'mklist (mapcar #'car binds))
		     (mappend #'mklist temps))
	 ,label
	 (if ,test
	     (return (progn ,@result)))
	 ,@body
	 (mvpsetq ,@(mapcan #'(lambda (b)
				(if (third b)
				    (list (car b)
					  (third b))))
			    binds))
	 (go ,label)))))

;; (mvdo* (((px py) (pos player) (move player mx my))
;; 	((x1 y1) (pos obj1) (move obj1 (- px x1)
;; 				  (- py y1)))
;; 	((x2 y2) (pos obj2) (move obj2 (- px x2)
;; 				  (- py y2)))
;; 	((mx my) (mouse-vector) (mouse-vector))
;; 	(win nil (touch obj1 obj2))
;; 	(lose nil (and (touch obj1 player)
;; 		       (touch obj2 player))))
;;        ((or win lose) (if win 'win 'lose))
;;        (clear)
;;        (draw obj1)
;;        (draw obj2)
;;        (draw player))

(defmacro mvdo* (parm-cl test-cl &body body)
  (mvdo-gen parm-cl parm-cl test-cl body))

(defun mvdo-gen (binds rebinds test body)
  (if (null binds)
      (let ((label (gensym)))
	`(prog nil
	  ,label
	  (if ,(car test)
	      (return (progn ,@(cdr test))))
	  ,@body
	  ,@(mvdo-rebind-gen rebinds)
	  (go ,label)))
      (let ((rec (mvdo-gen (cdr binds) rebinds test body)))
	(let ((var/s (caar binds)) (expr (cadar binds)))
	  (if (atom var/s)
	      `(let ((,var/s ,expr)) ,rec)
	      `(multiple-value-bind ,var/s ,expr ,rec))))))

(defun mvdo-rebind-gen (rebinds)
  (cond ((null rebinds) nil)
	((< (length (car rebinds)) 3)
	 (mvdo-rebind-gen (cdr rebinds)))
	(t
	 (cons (list (if (atom (caar rebinds))
			 'setq
			 'multiple-value-setq)
		     (caar rebinds)
		     (third (car rebinds)))
	       (mvdo-rebind-gen (cdr rebinds))))))

(defmacro mvdo (binds (test &rest result) &body body)
  (let ((label (gensym))
	(temps (mapcar #'(lambda (b)
			   (if (listp (car b))
			       (mapcar #'(lambda (x)
					   (gensym))
				       (car b))
			       (gensym)))
		       binds)))
    `(let ,(mappend #'mklist temps)
      (mvpsetq ,@(mapcan #'(lambda (b var)
			     (list var (cadr b)))
			 binds
			 temps))
      (prog ,(mapcar #'(lambda (b var) (list b var))
		     (mappend #'mklist (mapcar #'car binds))
		     (mappend #'mklist temps))
	 ,label
	 (if ,test
	     (return (progn ,@result)))
	 ,@body
	 (mvpsetq ,@(mapcan #'(lambda (b)
				(if (third b)
				    (list (car b)
					  (third b))))
			    binds))
	 (go ,label)))))



(defun shuffle (x y)
  (cond ((null x) y)
	((null y) x)
	(t (list* (car x) (car y)
		  (shuffle (cdr x) (cdr y))))))

(defmacro mvpsetq (&rest args)
  (let* ((pairs (group args 2))
	 (syms (mapcar #'(lambda (p)
			   (mapcar #'(lambda (x) (gensym))
				   (mklist (car p))))
		       pairs)))
    (labels ((rec (ps ss)
	       (if (null ps)
		   `(setq
		     ,@(mapcan #'(lambda (p s)
				   (shuffle (mklist (car p))
					    s))
			       pairs syms))
		   (let ((body (rec (cdr ps) (cdr ss))))
		     (let ((var/s (caar ps))
			   (expr (cadar ps)))
		       (if (consp var/s)
			   `(multiple-value-bind ,(car ss)
			     ,expr
			     ,body)
			   `(let ((,@(car ss) ,expr))
			     ,body)))))))
      (rec pairs syms))))

;; (let ((w 0) (x 1) (y 2) (z 3))
;;   (mvpsetq (w x) (values 'a 'b) (y z) (values w x) r 3)
;;   (list w x y z))








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A general let
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; let*, multiple-value-bind, destructuring-bind


(defmacro =~ (regex target-string &rest body)
  (with-gensyms (target match-start match-end reg-starts reg-ends) 
    `(macrolet (($ (index)
		  `(subseq ,',target
			   (aref ,',reg-starts (1- ,index))
			   (aref ,',reg-ends (1- ,index))))) 
       (let ((,target ,target-string))
	 (multiple-value-bind (,match-start ,match-end ,reg-starts ,reg-ends)
	     (scan ,regex ,target)
	   (when ,match-start
	     (let (($b 
		    (subseq ,target 0 ,match-start))
		   ($m 
		    (subseq ,target ,match-start ,match-end))
		   ($a 
		    (subseq ,target ,match-end)))
	       ,@body)))))))



(defmacro with (&whole exp &rest clauses)
  (let* ((type (car clauses)))
    (if (eql type :do)
	`(progn ,@ (cdr clauses))
	(multiple-value-bind (bind-forms rest)
	    ;; a slight problem. What if I want to bind a variable to keyword?
	    ;; tough luck?
	    (split-if #'keywordp (cdr clauses))
	  (aif (gethash type *with-modes*) (funcall it bind-forms `(with ,@rest))
	       (error "No with-mode is associated to ~S. In ~S" type exp))))))


(defparameter *with-modes* (make-hash-table))

(defmacro def-with-mode (keyword arg-list &rest body)
  `(setf (gethash ,keyword *with-modes*)
	 (fn ,arg-list ,@body))) 

(def-with-mode :mvbind (binds rest)
  (let ((binds (group binds 2)))
    (labels ((rec (binds)
	       (if (null binds)
		   rest
		   `(multiple-value-bind
			  ,(caar binds) ,(cadar binds)
		      ,(rec (cdr binds))))))
      (rec binds))))

(def-with-mode :dbind (binds rest)
  (let ((binds (group binds 2)))
    (labels ((rec (binds)
	       (if (null binds)
		   rest
		   `(destructuring-bind
			  ,(caar binds) ,(cadar binds)
		      ,(rec (cdr binds))))))
      (rec binds))))

(def-with-mode :let (binds rest)
  (let ((binds (group binds 2)))
    `(let ,binds ,rest)))

(def-with-mode :let* (binds rest)
  (let ((binds (group binds 2)))
    `(let* ,binds ,rest)))

(def-with-mode :flet (binds rest)
  `(flet ,binds ,rest))

(def-with-mode :labels (binds rest)
  `(labels ,binds ,rest))

(def-with-mode :flet* (binds rest)
  `(labels ,binds ,rest))

(def-with-mode :collector (binds rest)
  `(with-collectors ,(mapcar (fn (bind)
			       (aprogn (mklst bind)
				       (if (> (length it) 2)
					   (error "Illegal collector ~S." bind)
					   it)))
			     binds)
     ,rest))

(def-with-mode :pusher (binds rest) 
  `(with-collectors ,(mapcar (fn (bind)
			       (aprogn (mklst bind)
				       (cond ((> (length it) 2)
					      (error "Illegal pusher ~S" bind))
					     ((= (length it) 1) (append it '(nil t)))
					     (t (append it '(t))))))
			     binds)
     ,rest))


