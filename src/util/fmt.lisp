;; TODO
;;;; fmt should output to *standard-output* as expected.
;; (with-output-to-string (s)
;;   (let ((*standard-output* s))
;;     (print 1)
;;     (fmt 2 3 4)))

;; fmt should have a variant that suppresses printing nil.
;; ;; fmt*  this is to facilitate the use of control structure within fmt* directly

;; (fmt (when (= 1 1) 1)
;;      (when (> 1 1) 2)
;;      (fmt nil)
;;      (values)) => "1nilnilnil"

;; (fmt* (when (= 1 1) 1)
;;       (when (> 1 1) 2)
;;       (fmt nil)
;;       (values)) => "1nil"



(in-package :wisp-util)
(eval-when (:compile-toplevel :load-toplevel :execute)
  
  (cl-interpol:enable-interpol-syntax)  
  

;;; fmt macro
;;;; Issues
;; problem: (:<op> ...) and (fmt-<op> ...) don't mix well.

;; (eval-when (:compile-toplevel :load-toplevel :execute)
;;   (defparameter *fmt-stream* *standard-output*)
;;   (defparameter *fmt-context* (cons nil nil))
;;   (defparameter *fmt-ops* nil))

;;;; fmt globals
(defparameter *fmt-stream* *standard-output*)
(defparameter *fmt-ops* nil)
(defparameter *fmt-indent-em* 0)


;;;; fmt interface
(defmacro defmt-op (name args &body body)
  "Defines a new format operation macro that may be used within the fmt macro. The names has to be a keyword."
  (if (keywordp name) 
      `(-> (getf *fmt-ops* ,name) (rkr/fn ,args ,@body))
      ;;(setf (getf ,name *fmt-ops*) (fn/rkr ,args ,@body)) 
      (error "Invalid (keyword) name for format op: ~S" name)))

) ;; eval-when

(defun find-fmt-op (fmt-op) 
  (getf *fmt-ops* fmt-op))

;;  "No destructuring lambda arg"
(defparameter nil-options '(:nil :list :none))

(defun make-fmt-instructions (fmt-cmds &key (nil-option :nil))
  (flet ((select-nil-option (fmt-cmd)
	   (ecase nil-option
	     (:nil `(princ ,fmt-cmd *fmt-stream*))
	     (:none `(awhen ,fmt-cmd (princ it *fmt-stream*)))
	     (:list `(aif ,fmt-cmd
			  (princ it *fmt-stream*)
			  (princ "()" *fmt-stream*))))))
    (mapcar (fn (fmt-cmd)
	      (cond
		((keywordp fmt-cmd) `(fmt (,fmt-cmd)))
		((atom fmt-cmd) (select-nil-option fmt-cmd))
		(t
		 (aif (find-fmt-op (car fmt-cmd))
		      ;; I am throwing the enviornment away.
		      ;; ;; this is kinda ugly.
		      (apply (find-fmt-op (car fmt-cmd)) (cdr fmt-cmd))
		      (select-nil-option fmt-cmd)))))
	    fmt-cmds)))


(def/rkr/macro fmt (&key (> '*fmt-stream*) (nil-option :nil) &rest fmt-cmds) 
  (let* ((body (make-fmt-instructions fmt-cmds :nil-option nil-option))
	 (body `(macrolet ((fmt (&rest rest) `(progn ,@(make-fmt-instructions rest :nil-option ,nil-option))))
		  ,@body))) ;;rebind fmt so no extra logical blocks are created in embedded fmt calls 
    (case >
      ((nil) `(with-output-to-string (*fmt-stream*)
		(pprint-logical-block (*fmt-stream* nil) ,body)))
      (t `(let ((*fmt-stream* ,>))
	    (pprint-logical-block (*fmt-stream* nil) ,body))))))


(defmacro fmt* (&rest fnlist)
  `(fmt ,@(append '(:nil-option :none) fnlist)))

;;;; fmt-ops
(defmt-op :fmt (&rest rest)
  `(fmt ,@rest)) 

(defmt-op :% (&key (n 1))
  `(loop repeat ,n do (terpri *fmt-stream*)))

(defmt-op :& (&key (n 1))
  `(loop repeat ,n do (fresh-line *fmt-stream*)))


;;   "
;;   Difference from CLTL2:
;;   -'@' is always used as the flag for pad-left, regardless whether
;;     the arguments are specified in full form or with only the mincol
;;     argument.
;;   - Accepts multiple arguments."

(defmt-op :a (&key (as-nil t) (mincol 0) (colinc 1) (minpad 0) (padchar #\space) (pad-left nil) &rest args) 
  (let ((pad-left (if pad-left "@" ""))
	(as-nil (if as-nil "" ":")))
  `(format *fmt-stream* ,#?"~@\{~${mincol},${colinc},${minpad},'${padchar}${as-nil}${pad-left}A~}" ,@args)))

;; (fmt 
;;      (:a :mincol 5 1 2 3)
;;      (:&)
;;      (:a :mincol 10 1 2 3)
;;      (:% :n 3)
;;      (:a :mincol 15 1 2 3))

;; (fmt-a :mincol (+ 5 5) 10) ;; incorrect. fix.


;; (with-char-collector col
;;   (loop with upflag = t
;; 	for c across "little brown fox jumps over the fence"
;; 	if (and upflag (char/= c #\space #\newline))
;; 	do (col (char-upcase c)) and do (setq upflag nil)
;; 	else if (and (not upflag) (not (char/= c #\space #\newline)))
;; 	do (setq upflag t) and do (col c)
;; 	else do (col c)))



;; ;; ;; Probably a good idea to limit cap's usefulness.
;; ;; ;;;; Implement it by outputing the embedded fmt into a string, then diddle with that.
;; ;; (defmt-op :cap (&key (o :low) &rest rest) 
;; ;;   (let ((opt (ecase o
;; ;; 	       (:low "")
;; ;; 	       (:words ":")
;; ;; 	       (:first "@")
;; ;; 	       (:all ":@"))))
;; ;;   `(with-fmt-context (,#?"~${opt}(" "~)") 
;; ;;     (fmt ,@rest)))) 


;; (fmt (:cap :o :words
;; 	   (:a "how the fuck are you?"))
;;      (:%)
;;      (:cap :o :all
;; 	   (:a "how the fuck are you?"))
;;      (:%))

;; (format nil "~:(~A~)~%~:@(~A~)~%" "how the fuck are you?" "how the fuck are you?")


(defmt-op :if (test true false) 
  `(if ,test ,true ,false))

(defmt-op :lst (var-form &key infix &rest rest)
  ;; not really happy with the name, but can't think of anything better yet.
  (let ((args (gensym "REST"))
	(arg (car var-form)))
  `(loop for (,arg . ,args) = ,(cadr var-form) then ,args
    when ,arg do (fmt ,@rest)
    while ,args ,@(when infix `(do (fmt ,infix))))))

;; (fmt "("
;;      (:lst (e '((1 2) (3 4) (5 6))) :infix (:fmt " => ")
;;      (:a "<" (car e) "," (cadr e) ">"))
;;      ")")

(defmt-op :idn (&key (n 2) &rest body)
  `(progn
    (let ((*fmt-indent-em* *fmt-indent-em*))
      (setq *fmt-indent-em* (+ ,n *fmt-indent-em* ))
      (pprint-indent :block *fmt-indent-em* *fmt-stream*)
      (fmt (:_))
      (fmt ,@body)) 
    (pprint-indent :block *fmt-indent-em* *fmt-stream*)
    (fmt (:_))))

(defmt-op :tab ()
  ;; at least print a space to prevent nasty suprises
  ;; Do the right thing unless you know what you are doing.
  `(progn (pprint-tab :line-relative 5 1 *fmt-stream*)
	  (unless *print-pretty* (princ " " *fmt-stream*))))

(defmt-op :_ ()
  ;; at least print a space to prevent nasty suprises
  `(progn (pprint-newline :mandatory *fmt-stream*)
	  (unless *print-pretty* (princ " " *fmt-stream*))))



;; (fmt
;;  1
;;  (:idn 2 :tab 22
;;        (:idn 3 :tab 33 :tab 333
;; 	     (:idn 4 :_
;; 		   5 :tab 55 :tab 555 :tab 5555 :_
;; 		   6)
;; 	     3 :tab 33 :tab 333)
;;        2 :tab 22)
;;  1)



;; ;; What escape has to do is probably to turn pretty-print off so pprint-logical-block is ineffective.
;; (defmt-op :esc (&rest rest)		;
;;   `(with-new-fmt-context (fmt ,@rest)))

;; (deformatop :evl (&rest rest)
;;   `(progn ,@rest))

;; (fmt (:cap :o :words
;; 	   "abc"
;; 	   (:esc "efc" 
;; 		 (:a "blah foo bar"))
;; 	   "def"))



;;;; Case stream from sbcl
;; ;; :esc binds fresh context

;; (def-complex-format-interpreter #\( (colonp atsignp params directives)
;;   (let ((close (find-directive directives #\) nil)))
;;     (unless close
;;       (error 'format-error
;;              :complaint "no corresponding close paren"))
;;     (interpret-bind-defaults () params
;;       (let* ((posn (position close directives))
;;              (before (subseq directives 0 posn))
;;              (after (nthcdr (1+ posn) directives))
;;              (stream (make-case-frob-stream stream
;;                                             (if colonp
;;                                                 (if atsignp
;;                                                     :upcase
;;                                                     :capitalize)
;;                                                 (if atsignp
;;                                                     :capitalize-first
;;                                                     :downcase)))))
;;         (setf args (interpret-directive-list stream before orig-args args))
;;         after))))


;; (princ "how about this?" (sb-ext::make-case-frob-stream *standard-output* :capitalize))









;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 