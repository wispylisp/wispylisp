
;; (put 'destructure 'common-lisp-indent-function (get 'destructuring-bind 'common-lisp-indent-function))
;; (put 'dokids 'common-lisp-indent-function (get 'dotimes 'common-lisp-indent-function))

;; (put 'with-dialog-values 'common-lisp-indent-function (get 'with-slots 'common-lisp-indent-function))

;; Utterly clueless how indentation works.
(put 'defview 'common-lisp-indent-function (get 'defmethod 'common-lisp-indent-function))
(put 'defwethod 'common-lisp-indent-function (get 'defmethod 'common-lisp-indent-function))
(put 'defurlmap 'common-lisp-indent-function (get 'case 'common-lisp-indent-function))
(put 'fn 'common-lisp-indent-function (get 'lambda 'common-lisp-indent-function))
(put 'dbind 'common-lisp-indent-function (get 'destructuring-bind 'common-lisp-indent-function))
(put 'mvbind 'common-lisp-indent-function (get 'destructuring-bind 'common-lisp-indent-function))

(put 'def-generic-walker-methods 'common-lisp-indent-function (get 'progn 'common-lisp-indent-function))