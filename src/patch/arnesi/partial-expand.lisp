;; (in-package :it.bese.arnesi)
;;;; MACROLET

;;;; Stuff I wrote earlier for partial macro expansion
;; (defmethod unwalk-form ((form macrolet-form) &optional env &key black-macs white-macs)
;;   ;; We can't unwalk macrolet because, it contains closure objects.
;;   (let ((form (source form))
;; 	(parent (parent form)))
;;     (let ((maclet
;; 	   (with-form-object (macrolet macrolet-form :parent parent :source form
;; 				       :binds '()) 
;; 	     (dolist* ((name args &body body) (filter (lambda (mac)
;; 							;; if white-macs is non-nil, only macros appear in it will expand.
;; 							(and (or (and (null white-macs) (not (find (car mac) black-macs)))
;; 								 (find (car mac) white-macs))
;; 							     mac))
;; 						      (second form)))
;; 	       (let ((handler (eval
;; 			       ;; NB: macrolet arguments are a
;; 			       ;; destructuring-bind list, not a lambda list
;; 			       (with-unique-names (handler-args)
;; 				 `(lambda (&rest ,handler-args)
;; 				    (destructuring-bind ,args
;; 					,handler-args
;; 				      ,@body))))))
;; 		 (extend env :macrolet name handler)))
;; 	     (setf (binds macrolet) (nreverse (binds macrolet)))
;; 	     (multiple-value-setf ((body macrolet) nil (declares macrolet))
;; 	       (walk-implict-progn macrolet (cddr form) env :declare t)))))
;;       (if (= (length (body maclet)) 1)
;; 	  (unwalk-form (car (body maclet))
;; 		       env
;; 		       :black-macs black-macs
;; 		       :white-macs white-macs)
;; 	  (cons 'progn
;; 		(unwalk-forms (body maclet)
;; 			      env
;; 			      :black-macs black-macs
;; 			      :white-macs white-macs))))))

#+(or)
(unwalk-form (walk-form '(lambda (a b c)
			  (macrolet ((a () `(list a b c))
				    (b () `(list b c a))
				    (c () `(list c a b)))
			  (a) (b) (c))))
	     nil
	     :white-macs '(b c))
