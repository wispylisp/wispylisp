

(in-package :it.bese.arnesi)




(defun lookup (environment type name &key (error-p nil) (default-value nil))
  (loop
     for (.type .name . data) in environment 
     for depth = 0 then (incf depth)
     when (and (eql .type type) (eql .name name))
       return (values data t depth)
     finally
       (if error-p
           (error "Sorry, No value for ~S of type ~S in environment ~S found."
                  name type environment)
           (values default-value nil))))



(def-walker free-var-references (collect-free-vars)
  (free-variable-reference () (funcall collect-free-vars it)))

(def-walker local-var-references (collect-local-vars)
  (local-variable-reference () (funcall collect-local-vars it)))

(defun collect-free-vars (form)
  (with-collector (collect)
    (free-var-references form #'collect)
    (collect)))

(defun collect-local-vars (form)
  (with-collector (collect)
    (local-var-references form #'collect)
    (collect)))



(def-walker free-application (collect)
  (free-application-form () (funcall collect it)))

(def-walker local-application (collect)
  (local-application-form () (funcall collect it))
  (lexical-application-form () (funcall collect it)))


(defun collect-free-apps (form)
  (with-collector (collect)
    (free-application form #'collect)
    (collect)))

(defun collect-local-apps (form)
  (with-collector (collect)
    (local-application form #'collect)
    (collect)))





(defparameter *nesting-no-call/cc?* nil)


(defmacro no-call/cc (&rest body)
  "Used to get rid of any nesting no-call/cc, or those that don't appear within with-call/cc."
  `(progn ,@body))


(defwalker-handler no-call/cc (form parent env)
  "Makes the closure to be run in normal lisp mode.
`setf/q' doesn't work across the call boundary."
  (block fn-block
    (when *nesting-no-call/cc?* (return-from fn-block))
    ;; all nested no-call/cc are ignored
    (let ((*nesting-no-call/cc?* t)) 
      (with-form-object (no-call/cc no-call/cc-form :parent parent :source form) 
	(let* ((body-source (cons 'progn (cdr form)))
	       (body (walk-form (cons 'progn body-source) parent env))
	       (closure-body (walk-form body-source nil nil)) 
	       (local-vars (collect-local-vars body))
	       (free-vars (collect-free-vars closure-body))
	       (lex-vars (intersection local-vars free-vars :key #'name))
	       ;; ;; I found out later that I need all the special-variables found at runtime.
	       #+(or)(dyn-vars (set-difference free-vars lex-vars :key #'name))
	       #+(or)(free-apps (collect-free-apps closure-body))
	       #+(or)(local-apps (collect-local-apps body))
	       #+(or)(lex-apps (intersection local-apps free-apps :key #'operator))
	       lex-apps ;; use nil for now, so the machinery for analyzing local functions doesn't break.
	       )
	  #+(or)(when lex-apps (error "Damn, this is impossible! Can't refer to local functions estabalished in call/cc"))
	  ;; We find all the free variables in the closure body. Some of
	  ;; these are lexical variables established outside the closure
	  ;; body (others are special variables).  
	  ;;
	  ;; We need to pass in the lexical variables established outside
	  ;; the closure as arguments to the closure. This set of
	  ;; variables are those that appear to be free in the
	  ;; closure-body, but are actually local-variables. (The
	  ;; intersection of free-vars and local-vars).
	  ;;
	  ;; Geez. This is really hard to document. I can't explain it in
	  ;; a way that I can understand 3 months from now.
	  ;;
	  ;; Doesn't capture complicated dependencies.
	  (setf (lex-vars no-call/cc) lex-vars 
		;;(dyn-vars no-call/cc) dyn-vars
		(closure no-call/cc)
		(eval #+(or)(print `(lambda ,(mapcar #'name lex-vars)
				      ,(no-call/cc-fn-binds lex-apps body-source env))) 
		      `(lambda ,(mapcar #'name lex-vars)
			 ;; use macrolet to ignore all nesting no-call/cc's
			 ,(no-call/cc-fn-binds lex-apps body-source env))
		      ))))))) 



;; by the time I am done with this, I would've written apt-get...
;; I give up.
#+(or)
(defun find-lex-vars/fns (form env)
  (flet ((get-bind-fn-forms (lex-apps)
	   (remove-duplicates
	    (mapcar (compose #'parent #'car) ;; the parent of the function refered to is the bind-form.
		    ;; get all the function binding forms that are referred to within the closure.
		    (sort (mapcar (fn (lex-app) 
				    (multiple-value-list (lookup env :flet (operator lex-app))))
				  lex-apps)
			  (fn (a b)
			    ;; the innermost nested occurs last in the list
			    (> (third a) (third b))))))))
    (let* ((body-source (cons 'progn (cdr form)))
	   (body (walk-form (cons 'progn body-source) parent env))
	   (closure-body (walk-form body-source nil nil)) 
	   (local-vars (collect-local-vars body))
	   (free-vars (collect-free-vars closure-body))
	   (lex-vars (intersection local-vars free-vars :key #'name))
	   ;; ;; I found out later that I need all the special-variables found at runtime.
	   #+(or)(dyn-vars (set-difference free-vars lex-vars :key #'name))
	   (free-apps (collect-free-apps closure-body))
	   (local-apps (collect-local-apps body))
	   (lex-apps (intersection local-apps free-apps :key #'operator))
	   (lex-fns (get-bind-fn-forms lex-apps)))
      (mappend lex-apps)
      )))





(defun no-call/cc-fn-binds (lex-apps body env)
  "Binds the local functions that are needed in the body of the
no-call/cc closure. In correct nesting order. "
  (labels ((bind-fns (fns body)
	     (if (null fns)
		 body
		 (let ((fn (car fns)))
		   (if (eql (type-of fn) 'labels-form)
		       ;; can probably do a bit better by only including needed functions
		       `(labels ,(mapcar (fn (bind) (list* (car bind)
							   (cdr (source (cdr bind)))))
					 (binds fn))
			  ,(bind-fns (cdr fns) body))
		       `(flet ,(mapcar (fn (bind) (list* (car bind)
							 (cdr (source (cdr bind)))))
				       (binds fn))
			  ,(bind-fns (cdr fns) body))))))
	   (get-bind-fn-forms (lex-apps)
	     (remove-duplicates
	      (mapcar (compose #'parent #'car) ;; the parent of the function refered to is the bind-form.
		      ;; get all the function binding forms that are referred to within the closure.
		      (sort (mapcar (fn (lex-app) 
				      (multiple-value-list (lookup env :flet (operator lex-app))))
				    lex-apps)
			    (fn (a b)
			      ;; the innermost nested occurs last in the list
			      (> (third a) (third b))))))))
    (bind-fns (get-bind-fn-forms lex-apps) body)))



(defmethod evaluate/cc ((no-call/cc no-call/cc-form) lex-env dyn-env k)
  "Establishes the environment for the no-call/cc closure."
  (with-slots (lex-vars dyn-vars closure) no-call/cc
    (let* ((fake-k (list (fn () #'identity)))
	   (eval-val (rcurry #'evaluate/cc lex-env dyn-env fake-k))
	   ;; need to gather all special-variables.
	   (dyn-cons (filter (fn (cons)
			       (aif (eql (car cons) :let)
				    (cdr cons)
				    nil))
			     dyn-env))
	   (lex-vals (mapcar eval-val lex-vars)))
      ;;(print (list lex-vars dyn-vars)) 
      (kontinue k (progv (mapcar #'car dyn-cons) (mapcar #'cdr dyn-cons)
		    (apply closure lex-vals))))))


