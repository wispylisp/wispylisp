(in-package :it.bese.arnesi)
;;;; I wanted to have a general way to analyze the code-tree.
;;;; This is the half-assed result. Good enough for now.



(defclass no-call/cc-form (form)
  ((closure :initarg :closure :accessor closure)
   (lex-vars :initarg :lex-vars :accessor lex-vars)
   #+(or)(dyn-vars :initarg :dyn-vars :accessor dyn-vars)))
  
(defmacro def-walker (name fnlist &rest cases)
  ;; assume fnlist are all required args.
  (let ((fn-name (symcat name '-method)))
    `(progn
       (defmethod ,fn-name (form ,@fnlist))
       (flet ((rec (form) (generic-walker form ,fn-name)))
	 (def-walker-methods ,fn-name ,fnlist ,@cases))
       (defun ,name (form ,@fnlist)
	 (generic-walker form (rcurry (function ,fn-name) ,@fnlist))))))

(defmacro def-walker-methods (fn-name fn-list &rest cases)
  (with-unique-names (form)
    `(progn
       (defgeneric ,fn-name (form ,@fn-list)) 
       ,@(mapcar (lambda (case)
		   (let* ((type (car case)) 
			  (slots (cadr case))
			  (body (cddr case)))
		     `(defmethod ,fn-name ((it ,type) ,@fn-list)
			(with-slots ,slots it
			  ,@body))))
		 cases))))


(defmacro def-generic-walker-methods (&rest cases)
  `(macrolet ((rec (form) `(generic-walker ,form fn)))
     (macrolet ((rec* (forms) `(dolist (form ,forms)
				 (rec form))))
       (fmakunbound 'generic-walker)
       (defgeneric generic-walker (form fn))
       ,@(mapcar (lambda (case)
		   (let* ((type (car case)) 
			  (slots (cadr case))
			  (body (cddr case)))
		     `(defmethod generic-walker ((it ,type) fn)
			(with-slots ,slots it
			  (funcall fn it)
			  ,@body))))
		 cases) 
       (defmethod generic-walker (form fn)
	 (funcall fn form)))))


(def-generic-walker-methods 
  (application-form (operator arguments)
		    (rec operator)
		    (rec* arguments)) 
  (lambda-function-form (arguments body)
			(rec* arguments)
			(rec* body))
  (function-object-form (name)
			(rec name))
  #+(or)(function-argument-form () (funcall fn it))
  ;; (required-function-argument-form (name) (funcall fn it))
  #+(or)(specialized-function-argument-form (name specializer)
					    (if (eq specializer t)
						name
						`(,name ,specializer))) 
  (optional-function-argument-form (default-value)
				   (rec default-value)) 
  (keyword-function-argument-form (default-value)
				  (rec default-value))
  #+(or)(allow-other-keys-function-argument-form ()
						 '&allow-other-keys) 
  #+(or)(rest-function-argument-form (name)
				     name)
  
;;;; BLOCK/RETURN-FROM 
  (block-form (body)
	      (rec* body)) 
  (return-from-form (result)
		    (rec result)) 
;;;; CATCH/THROW 
  (catch-form (tag body)
	      (rec tag)
	      (rec* body)) 
  (throw-form (tag value)
	      (rec tag)
	      (rec value)) 
;;;; EVAL-WHEN 
  (eval-when-form ()
		  (error "Sorry, EVAL-WHEN not yet implemented.")) 
;;;; IF 
  (if-form (consequent then else)
	   (rec consequent)
	   (rec then)
	   (rec else)) 
;;;; FLET/LABELS 
  (function-binding-form (binds body)
			 (dolist (bind binds)
			   (rec (cdr bind)))
			 (rec* body)) 
;;;; LET/LET* 
  (variable-binding-form (binds body) 
			 (dolist (bind binds)
			   (rec (cdr bind)))
			 (rec* body)) 
;;;; LOAD-TIME-VALUE 
  (load-time-value-form (value read-only-p)
			(rec value)) 
;;;; LOCALLY 
  (locally-form (body)
		(rec* body)) 
;;;; MULTIPLE-VALUE-CALL 
  (multiple-value-call-form (func arguments)
			    (rec func)
			    (rec* arguments)) 
;;;; MULTIPLE-VALUE-PROG1 
  (multiple-value-prog1-form (first-form other-forms)
			     (rec first-form)
			     (rec* other-forms)) 
;;;; PROGN 
  (progn-form (body)
	      (rec* body))
;;;; PROGV 
  (progv-form (body vars-form values-form)
	      (rec vars-form)
	      (rec values-form)
	      (rec* body)) 
;;;; SETQ 
  (setq-form (value)
	     (rec value))
;;;; SYMBOL-MACROLET 
  (symbol-macrolet-form ()
			;; We can't unwalk macrolet because, it modifies the body
			;; expression, when its created.
			(error "Sorry, SYMBOL-MACROLET not yet implemented."))
  (macrolet-form ()
		 ;; We can't unwalk macrolet because, it modifies the body
		 ;; expression, when its created.
		 (rec* (body it)))
;;;; TAGBODY/GO
  (tagbody-form (body)
		(rec* body)) 
;;;; THE 
  (the-form (type-form value)
	    (rec type-form)
	    (rec value)) 
;;;; UNWIND-PROTECT 
  (unwind-protect-form (protected-form cleanup-form)
		       (rec protected-form)
		       (rec* cleanup-form))
  ;;;; no-call/cc
  (no-call/cc-form (lex-vars dyn-vars closure)
		   (rec lex-vars)
		   (rec dyn-vars)
		   (rec closure))
  )




