(in-package :it.bese.arnesi)

(defwalker-handler +atom-marker+ (form parent env)
  (declare (special *macroexpand*))
  (cond
    ((not (or (symbolp form) (consp form)))
     (make-instance 'constant-form :value form
                    :parent parent :source form))
    ((and (not (boundp form)) ;; not a global variable.
	  (lookup env :let form))
     (make-instance 'local-variable-reference :name form
                    :parent parent :source form))
    ((lookup env :lexical-let form)
     (make-instance 'local-lexical-variable-reference :name form
                    :parent parent :source form))
    ((lookup env :symbol-macrolet form)
     (walk-form (lookup env :symbol-macrolet form) parent env))
    ((nth-value 1 (macroexpand-1 form))
     ;; a globaly defined symbol-macro
     (walk-form (macroexpand-1 form) parent env))
    (t
     (when (and *warn-undefined*
                (not (boundp form)))
       (warn 'undefined-variable-reference :name form))
     (make-instance 'free-variable-reference :name form
                    :parent parent :source form))))




;; (defgeneric unwalk-form (form &key
;; 			      (exclude-macros nil exclude-macros-p)
;; 			      (include-macros nil include-macros-p)))

;; (defun unwalk-form ((form t)) 
;;   (check-type form form)
;;   (source form))

;; (defun partial-expand (form)
;;   (let ((form (walk-form form)))
;;     ))

;; #+(or)
;; (defunwalker-handler macrolet-form ()
;;   ;; We can't unwalk macrolet because, it contains closure objects.
;;   (error "Sorry, MACROLET not yet implemented."))

;; (defunwalker-handler macrolet-form (parent source)
;;   ;; We can't unwalk macrolet because, it contains closure objects. 
;;   source)

;; #+(or)
;; (defunwalker-handler symbol-macrolet-form ()
;;   ;; We can't unwalk macrolet because, it modifies the body
;;   ;; expression, when its created.
;;   (error "Sorry, MACROLET not yet implemented."))

;; (defunwalker-handler symbol-macrolet-form ()
;;   ;; We can't unwalk macrolet because, it modifies the body
;;   ;; expression, when its created.
;;   (error "Sorry, MACROLET not yet implemented."))


