;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; These will never work
;;
;; No good way to convert closure/cc to ordinary closures.
;; (with-call/cc (let ((a (lambda () 1))) 
;; 		  (no-call/cc (funcall a))))
;;
;; (with-call/cc (let ((a (lambda () 1)))
;; 		(flet ((a () (funcall a)))
;; 		  (no-call/cc (a)))))

(in-package :arnesi)

(with-call/cc (no-call/cc (list 1 (no-call/cc 2)))) 
(1 2)

(with-call/cc (no-call/cc 1))
1

(type-of (with-call/cc (no-call/cc #'(lambda ()))))
function

(defparameter c 3)

(defun d ()
  (declare (special d)) 
  d)

(let ((a 1)) 
  (with-call/cc (equal a
		       ;; local-lexical-reference
		       (no-call/cc a))))
t

(with-call/cc (equal c
		     ;; global-variable
		     (no-call/cc c)))
t

(with-call/cc (let ((d 2))
		(declare (special d))
		(equal (d)
		       ;; special-variable
		       (no-call/cc (d)))))
t

(let ((d 2))
  (declare (special d))
  (with-call/cc (equal (d)
		       ;; special-variable
		       (no-call/cc (d)))))
t

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Better not working than half-working.
;; To get it all working, I have to analyze the binding functions to
;; determine their dependencies to the local environment, which is
;; waay too much work.  
;;
;; (with-call/cc (flet ((a () 1))
;; 		(labels ((b () 2)
;; 			 (bb () (b)))
;; 		  (equal (list (a) (bb) (b))
;; 			 (no-call/cc (list (a) (bb) (b)))))))

;; (with-call/cc (flet ((a () 1))
;; 		(labels ((b () 2)
;; 			 (bb () (b)))
;; 		  (flet ((b () 3))
;; 		    (no-call/cc (list (a) (bb) (b)))))))

;; (with-call/cc (let ((a 1))
;; 		(flet ((a () a))
;; 		  (no-call/cc (a)))))

;; (with-call/cc
;;   (let ((a 1))
;;     (flet ((a () a))
;;       (a))))



;; (flet ((a () 2)))

;; (flet ((a () 1))
;;   (labels ((b () 2)
;; 	   (bb () (b)))
;;     (flet ((b () 3))
;;       (with-call/cc 
;; 	(no-call/cc (list (a) (bb) (b)))))))

;; (flet ((a () 1))
;;   (labels ((b () 2)
;; 	   (bb () (b)))
;;     (flet ((b () 3))
;;       (with-call/cc 
;; 	(list (a) (bb) (b))))))


