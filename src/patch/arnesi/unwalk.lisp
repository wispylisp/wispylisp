

(in-package :it.bese.arnesi)

(defun lookup (environment type name &key (error-p nil) (default-value nil))
  (loop
     for (.type .name . data) in environment 
     for depth = 0 then (incf depth)
     when (and (eql .type type) (eql .name name))
       return (values data t depth)
     finally
       (if error-p
           (error "Sorry, No value for ~S of type ~S in environment ~S found."
                  name type environment)
           (values default-value nil))))

(use-package :wisp-util)












(def-walker free-var-references (collect-free-vars)
  (free-variable-reference () (funcall collect-free-vars it)))

(def-walker local-var-references (collect-local-vars)
  (local-variable-reference () (funcall collect-local-vars it)))

(defun collect-free-vars (form)
  (with-collector (collect)
    (free-var-references form #'collect)
    (collect)))

(defun collect-local-vars (form)
  (with-collector (collect)
    (local-var-references form #'collect)
    (collect)))


(def-walker free-application (collect)
  (free-application-form () (funcall collect it)))

(def-walker local-application (collect)
  (local-application-form () (funcall collect it))
  (lexical-application-form () (funcall collect it)))


(defun collect-free-apps (form)
  (with-collector (collect)
    (free-application form #'collect)
    (collect)))

(defun collect-local-apps (form)
  (with-collector (collect)
    (local-application form #'collect)
    (collect)))



(defclass no-call/cc-form (form)
  ((closure :initarg :closure :accessor closure)
   (lex-vars :initarg :lex-vars :accessor lex-vars)
   #+(or)(dyn-vars :initarg :dyn-vars :accessor dyn-vars)))




(defwalker-handler no-call/cc (form parent env)
  "Makes the closure to be run in normal lisp mode.
`setf/q' doesn't work across the call boundary."
  (with-form-object (no-call/cc no-call/cc-form :parent parent :source form) 
    (let* ((body-source (cons 'progn (cdr form)))
	   (body (walk-form (cons 'progn body-source) parent env))
	   (closure-body (walk-form body-source nil nil)) 
	   (local-vars (collect-local-vars body))
	   (free-vars (collect-free-vars closure-body))
	   (lex-vars (intersection local-vars free-vars :key #'name))
	   ;; ;; I found out later that I need all the special-variables found at runtime.
	   #+(or)(dyn-vars (set-difference free-vars lex-vars :key #'name))
	   (free-apps (collect-free-apps closure-body))
	   (local-apps (collect-local-apps body))
	   (lex-apps (intersection local-apps free-apps :key #'operator))) 
      ;; the intersection of local-vars and free-vars are the lexical
      ;; arguments of the closure  that needs to be created.
      ;;
      ;; lexical shadowing should work even though set-operations
      ;; don't gaurantee ordering. It only depends on `lookup' performing
      ;; as expected.
      ;; 
      (setf (lex-vars no-call/cc) lex-vars 
	    ;;(dyn-vars no-call/cc) dyn-vars
	    (closure no-call/cc)
	    (eval (print `(lambda ,(mapcar #'name lex-vars)
			    ,(no-call/cc-fn-binds lex-apps body-source env)))
		  #+(or)`(lambda ,(mapcar #'name lex-vars)
			   ,(bind-fns (remove-duplicates
				       (mapcar (fn (app) (parent (lookup env :flet (operator app))))
					       lex-apps))
				      body-source))
		  )))))

(defun no-call/cc-fn-binds (lex-apps body env)
  "Makes the local functions that are needed in the body of the no-call/cc closure."
  (labels ((bind-fns (fns body)
	     (if (null fns)
		 body
		 (let ((fn (car fns)))
		   (if (eql (type-of fn) 'labels-form)
		       ;; can probably do a bit better by only including needed functions
		       `(labels ,(mapcar (fn (bind) (list* (car bind)
							   (cdr (source (cdr bind)))))
					 (binds fn))
			  ,(bind-fns (cdr fns) body))
		       `(flet ,(mapcar (fn (bind) (list* (car bind)
							 (cdr (source (cdr bind)))))
				       (binds fn))
			  ,(bind-fns (cdr fns) body))))))
	   (get-bind-fn-forms (lex-apps)
	     (remove-duplicates
	      (mapcar (compose #'parent #'car) ;; the parent of the function refered to is the bind-form.
		      ;; get all the function binding forms that are referred to within the closure.
		      (sort (mapcar (fn (lex-app) 
				      (multiple-value-list (lookup env :flet (operator lex-app))))
				    lex-apps)
			    (fn (a b)
			      ;; the innermost nested occurs last in the list
			      (> (third a) (third b))))))))
    (bind-fns (get-bind-fn-forms lex-apps) body)))



(defmethod evaluate/cc ((no-call/cc no-call/cc-form) lex-env dyn-env k)
  (with-slots (lex-vars dyn-vars closure) no-call/cc
    (let* ((fake-k (list (fn () #'identity)))
	   (eval-val (rcurry #'evaluate/cc lex-env dyn-env fake-k))
	   ;; need to gather all special-variables.
	   (dyn-cons (filter (fn (cons)
			       (aif (eql (car cons) :let)
				    (cdr cons)
				    nil))
			     dyn-env))
	   (lex-vals (mapcar eval-val lex-vars)))
      ;;(print (list lex-vars dyn-vars)) 
      (kontinue k (progv (mapcar #'car dyn-cons) (mapcar #'cdr dyn-cons)
		    (apply closure lex-vals))))))



(with-call/cc (no-call/cc 1))

(type-of (with-call/cc (no-call/cc #'(lambda ()))))

(defparameter c 3)

(defun d ()
  (declare (special d)) 
  d)

(let ((a 1)) 
  (with-call/cc (equal a
		       ;; local-lexical-reference
		       (no-call/cc a))))

(with-call/cc (equal c
		     ;; global-variable
		     (no-call/cc c)))

(with-call/cc (let ((d 2))
		(declare (special d))
		(equal (d)
		       ;; special-variable
		       (no-call/cc (d)))))
(let ((d 2))
  (declare (special d))
  (with-call/cc (equal (d)
		       ;; special-variable
		       (no-call/cc (d)))))


(with-call/cc (flet ((a () 1))
		(labels ((b () 2)
			 (bb () (b)))
		  (equal (list (a) (bb) (b))
			 (no-call/cc (list (a) (bb) (b)))))))

(with-call/cc (flet ((a () 1))
		(labels ((b () 2)
			 (bb () (b)))
		  (flet ((b () 3))
		    (no-call/cc (list (a) (bb) (b)))))))

;; (flet ((a () 1))
;;   (labels ((b () 2)
;; 	   (bb () (b)))
;;     (flet ((b () 3))
;;       (with-call/cc 
;; 	(no-call/cc (list (a) (bb) (b)))))))

;; (flet ((a () 1))
;;   (labels ((b () 2)
;; 	   (bb () (b)))
;;     (flet ((b () 3))
;;       (with-call/cc 
;; 	(list (a) (bb) (b))))))



;;;; I wanted to have a general way to analyze the code-tree.
;;;; This is the half-assed result. Good enough for now.

(defmacro def-walker (name fnlist &rest cases)
  ;; assume fnlist are all required args.
  (let ((fn-name (symcat name '-method)))
    `(progn
       (defmethod ,fn-name (form ,@fnlist))
       (flet ((rec (form) (generic-walker form ,fn-name)))
	 (def-walker-methods ,fn-name ,fnlist ,@cases))
       (defun ,name (form ,@fnlist)
	 (generic-walker form (rcurry (function ,fn-name) ,@fnlist))))))

(defmacro def-walker-methods (fn-name fn-list &rest cases)
  (with-unique-names (form)
    `(progn
       (defgeneric ,fn-name (form ,@fn-list)) 
       ,@(mapcar (lambda (case)
		   (let* ((type (car case)) 
			  (slots (cadr case))
			  (body (cddr case)))
		     `(defmethod ,fn-name ((it ,type) ,@fn-list)
			(with-slots ,slots it
			  ,@body))))
		 cases))))


(defmacro def-generic-walker-methods (&rest cases)
  `(macrolet ((rec (form) `(generic-walker ,form fn)))
     (macrolet ((rec* (forms) `(dolist (form ,forms)
				 (rec form))))
       (fmakunbound 'generic-walker)
       (defgeneric generic-walker (form fn))
       ,@(mapcar (lambda (case)
		   (let* ((type (car case)) 
			  (slots (cadr case))
			  (body (cddr case)))
		     `(defmethod generic-walker ((it ,type) fn)
			(with-slots ,slots it
			  (funcall fn it)
			  ,@body))))
		 cases) 
       (defmethod generic-walker (form fn)
	 (funcall fn form)))))


(def-generic-walker-methods 
  (application-form (operator arguments)
		    (rec operator)
		    (rec* arguments)) 
  (lambda-function-form (arguments body)
			(rec* arguments)
			(rec* body))
  (function-object-form (name)
			(rec name))
  #+(or)(function-argument-form () (funcall fn it))
  ;; (required-function-argument-form (name) (funcall fn it))
  #+(or)(specialized-function-argument-form (name specializer)
					    (if (eq specializer t)
						name
						`(,name ,specializer))) 
  (optional-function-argument-form (default-value)
				   (rec default-value)) 
  (keyword-function-argument-form (default-value)
				  (rec default-value))
  #+(or)(allow-other-keys-function-argument-form ()
						 '&allow-other-keys) 
  #+(or)(rest-function-argument-form (name)
				     name)
  
;;;; BLOCK/RETURN-FROM 
  (block-form (body)
	      (rec* body)) 
  (return-from-form (result)
		    (rec result)) 
;;;; CATCH/THROW 
  (catch-form (tag body)
	      (rec result)
	      (rec* body)) 
  (throw-form (tag value)
	      (rec tag)
	      (rec value)) 
;;;; EVAL-WHEN 
  (eval-when-form ()
		  (error "Sorry, EVAL-WHEN not yet implemented.")) 
;;;; IF 
  (if-form (consequent then else)
	   (rec consequent)
	   (rec then)
	   (rec else)) 
;;;; FLET/LABELS 
  (function-binding-form (binds body)
			 (dolist (bind binds)
			   (rec (cdr bind)))
			 (rec* body)) 
;;;; LET/LET* 
  (variable-binding-form (binds body) 
			 (dolist (bind binds)
			   (rec (cdr bind)))
			 (rec* body)) 
;;;; LOAD-TIME-VALUE 
  (load-time-value-form (value read-only-p)
			(rec value)) 
;;;; LOCALLY 
  (locally-form (body)
		(rec* body)) 
;;;; MULTIPLE-VALUE-CALL 
  (multiple-value-call-form (func arguments)
			    (rec func)
			    (rec* arguments)) 
;;;; MULTIPLE-VALUE-PROG1 
  (multiple-value-prog1-form (first-form other-forms)
			     (rec first-form)
			     (rec* other-forms)) 
;;;; PROGN 
  (progn-form (body)
	      (rec* body))
;;;; PROGV 
  (progv-form (body vars-form values-form)
	      (rec vars-form)
	      (rec values-form)
	      (rec* body)) 
;;;; SETQ 
  (setq-form (value)
	     (rec value))
;;;; SYMBOL-MACROLET 
  (symbol-macrolet-form ()
    ;; We can't unwalk macrolet because, it modifies the body
    ;; expression, when its created.
    (error "Sorry, MACROLET not yet implemented.")) 
;;;; TAGBODY/GO
  (tagbody-form (body)
		(rec* body)) 
;;;; THE 
  (the-form (type-form value)
	    (rec type-form)
	    (rec value)) 
;;;; UNWIND-PROTECT 
  (unwind-protect-form (protected-form cleanup-form)
		       (rec protected-form)
		       (rec* cleanup-form))
  ;;;; no-call/cc
  (no-call/cc-form (lex-vars dyn-vars closure)
		   (rec lex-vars)
		   (rec dyn-vars)
		   (rec closure))
  )










;;;; MACROLET

;;;; Stuff I wrote earlier for partial macro expansion
;; (defmethod unwalk-form ((form macrolet-form) &optional env &key black-macs white-macs)
;;   ;; We can't unwalk macrolet because, it contains closure objects.
;;   (let ((form (source form))
;; 	(parent (parent form)))
;;     (let ((maclet
;; 	   (with-form-object (macrolet macrolet-form :parent parent :source form
;; 				       :binds '()) 
;; 	     (dolist* ((name args &body body) (filter (lambda (mac)
;; 							;; if white-macs is non-nil, only macros appear in it will expand.
;; 							(and (or (and (null white-macs) (not (find (car mac) black-macs)))
;; 								 (find (car mac) white-macs))
;; 							     mac))
;; 						      (second form)))
;; 	       (let ((handler (eval
;; 			       ;; NB: macrolet arguments are a
;; 			       ;; destructuring-bind list, not a lambda list
;; 			       (with-unique-names (handler-args)
;; 				 `(lambda (&rest ,handler-args)
;; 				    (destructuring-bind ,args
;; 					,handler-args
;; 				      ,@body))))))
;; 		 (extend env :macrolet name handler)))
;; 	     (setf (binds macrolet) (nreverse (binds macrolet)))
;; 	     (multiple-value-setf ((body macrolet) nil (declares macrolet))
;; 	       (walk-implict-progn macrolet (cddr form) env :declare t)))))
;;       (if (= (length (body maclet)) 1)
;; 	  (unwalk-form (car (body maclet))
;; 		       env
;; 		       :black-macs black-macs
;; 		       :white-macs white-macs)
;; 	  (cons 'progn
;; 		(unwalk-forms (body maclet)
;; 			      env
;; 			      :black-macs black-macs
;; 			      :white-macs white-macs))))))

#+(or)
(unwalk-form (walk-form '(lambda (a b c)
			  (macrolet ((a () `(list a b c))
				    (b () `(list b c a))
				    (c () `(list c a b)))
			  (a) (b) (c))))
	     nil
	     :white-macs '(b c))
