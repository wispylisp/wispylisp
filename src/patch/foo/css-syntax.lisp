

;;;; 5.2.1 Grouping
;; H1, H2, H3 { font-family: sans-serif }
(:or h1 h2 h3)

;;; 5.3 Universal selector
;; * p
(* p) 


;;; 5.5 Descendant selectors 
;; ul ol ul em {color: gray;} 
(ul ol ul em)


;;; 5.6 Child selectors 
;; h1 > strong {color: red;} ;; children
(h1 > strong)

;; DIV OL>LI P 
(div ol > li p)

;;; 5.7 Adjacent sibling selectors
;; h1 + p {margin-top: 0;} ;; adjacent 
(h1 + p) 


;;; 5.8 Attribute Selector
;;;; 5.8.1 Matching attributes and attribute values

;; [att] 
;; Match when the element sets the "att" attribute, whatever the value of the attribute. 
;; [att=val] 
;; Match when the element's "att" attribute value is exactly "val". 
;; [att~=val] 
;; Match when the element's "att" attribute value is a space-separated list of "words", one of which is exactly "val". If this selector is used, the words in the value must not contain spaces (since they are separated by spaces). 
;; [att|=val] 
;; Match when the element's "att" attribute value is a hyphen-separated list of "words", beginning with "val". The match always starts at the beginning of the attribute value. This is primarily intended to allow language subcode matches (e.g., the "lang" attribute in HTML) as described in RFC 1766 ([RFC1766]).

;; *[LANG|="en"] { color : red }
(ref * (- LANG "en")) ;; hyphen separated

;; A[rel~="copyright"] 
(ref a (_ rel "copyright")) ;;space separated 

;; foo[bar][jar~="mar"][dada|="gaga"][qux="deluxe"]
(ref foo bar (_ jar "mar") (- dada "gaga") (dada "gaga"))


;;;; 5.8.3 Class selectors
;; .pastoral.marine
.pastoral.marine

;; P.pastoral.marine { color: green } 
p.pastoral.marine

;; div p.pastoral.marine { color: green } 
(div p.pastoral.marine)

;; td.sidebar a:link {color: white;} 
(td.sidebar a:link:hover)

;; * .warning
(* .warning)

;; Note. CSS gives so much power to the "class" attribute, that authors could conceivably design their own "document language" based on elements with almost no associated presentation (such as DIV and SPAN in HTML) and assigning style information through the "class" attribute. Authors should avoid this practice since the structural elements of a document language often have recognized and accepted meanings and author-defined classes may not.
;; ;; What? Isn't this the whole idea? No presentation information in the HTML structure.

;;; 5.9 ID selectors 
;; H1#chapter1 { text-align: center }
(h1 $ chapter1)
;; rather unfortunate $ happens to sit beside #
;; ;; prototype chose $ for get-element-by-id Perhaps for the same reason?

;;; 5.11 Pseudo-classes
:first-child

:link
:visited

:hover
:active
:focus

:first-line
:first-letter

:before
:after


;;;; Combination

;; Should I allow different kinds of selectors in the same consp?
(a b ~ moo mah :link :hover)
;; or
((a b ~ moo mah) :link :hover)
;; a b.moo.mah ??
(a (b ~ moo mah) :link :hover) ;; illegal. Pseudo-classes must follow one single selector
(a ((b ~ moo mah) :link :hover))
;; a b.moo.mah:link:hover

;; Maybe the best way to deal with pseudo classes is to move them within cascading.
;; ;; Pseudo classes always at the end of selector. No exceptions. Else doesn't make sense.

;;;; Parsing Complex selectors

;; Import
;; Group
;; ;; Pseudo-classes (postfix)
;; ;; ;; keyword 
;; ;; Selectors
;; ;; ;; ID (postfix?)
;; ;; ;; ;; $
;; ;; ;; Structure (prefix?)
;; ;; ;; ;;  > +
;; ;; ;; Attribute (infix?)
;; ;; ;; ;;  ~ @

;; postfix is appropriate when nothing else can follow
;; prefix is appropriate to denote structural relationship
;; infix is appropriate when there can be arbitrarily many operands on either sides of the operator.


;; ;; Can there be selectors like these??
;; a.b.c[foo][bar][qux]
(:ref a.b.c foo bar qux)
;; c[foo][bar][qux].b.a
(:dot (:ref c foo bar qux)
      b a)

;; .c[foo][bar][qux].b.a
(:dot (:ref .c foo bar qux)
      b a)



;; ;; forget it, require ~ before @.


div.c.d > span.foo span + a:link:visted

div.c.d:hover > span.foo span + a:link:visted
 
(> div.c.d
   (+ (span.foo span) div:hover))

div.c.d > span.foo span + div:hover

(+ (> (span.foo span)
      div:hover)
   div.c.d)

span.foo span > div:hover + div.c.d

(+ div.c.d
   (> (span.foo span) div:hover))

div.c.d + span.foo span > div:hover

(+ div.c.d$id1
   (> (span.foo$id2 span) div$id3:hover))

div.c.d#id1 + span.foo#id2 span > div#id3:hover


(:sib div.c.d$id1
   (:kid (span.foo$id2 span) div$id3:hover))

;; I am already being cryptic with :sib and :kid, what's wrong with + and >?
;; ;; It's just a little annoying for them to remind me of mathematical symbols.
;; ;; Plus, they don't really mean anything. 



