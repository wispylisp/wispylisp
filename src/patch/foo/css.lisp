;; TODO: Make cascading more structurally explicit 



;;
;; Copyright (c) 2005, Gigamonkeys Consulting All rights reserved.
;;

(in-package :com.gigamonkeys.foo.css)
(use-package :wisp-util)

(defclass css (language)
  ()
  (:default-initargs
   :special-operator-symbol 'css-special-operator
    :macro-symbol 'css-macro
    :input-readtable (copy-readtable)
    :input-package (find-package :keyword)
    :output-file-type "css"))

(defparameter *css* (make-instance 'css))

(defun compile-css (input &key (output (make-pathname :type "css" :defaults input)))
  (assert (not (equal (pathname input) (pathname output))))
  (with-open-file (in input)
    (with-open-file (*text-output* output :direction :output :if-exists :supersede)
      (format *text-output* "/* Generated at ~a from ~a. */~2%" (format-iso-8601-time (get-universal-time)) (truename in))
      (loop for form = (read in nil in)
	 while (not (eql form in)) do
	   (emit-css form)))))

(defun emit-css (sexp) 
  (process *css* (get-pretty-printer) sexp nil))

(defmacro css (&whole whole &body body)
  (declare (ignore body))
  `(macrolet ((css (&body body) 
		(codegen-text (sexp->ops *css* body nil) ,*pretty*)))
     ,@(if *pretty*
	   `((let ((*text-pretty-printer* (get-pretty-printer))) ,whole))
	   `(,whole))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Language implementation


(defmethod identifier ((language css) form)
  (when (eql (car form) :import) :import))

(defmethod sexp-form-p ((language css) form)
  (or (css-atom? form) (consp form)))

(defmethod embeddable-value-form ((language css) form)
  `(string-downcase (princ-to-string ,form)))

(defun css-atom? (form)
  ;; what about nil??
  (atom form))

(defmethod process-sexp ((language css) processor form environment)
  (declare (ignore environment)) 
  (cond
    ((css-atom? form)
     (raw-string processor (substitute #\# #\$ (^string form)) t))
    ((eql (first form) :import)
     (emit-css-import processor form))
    (t 
     (process-non-import-css processor form))))


(defun emit-css-import (processor sexp)
  (let ((url (second sexp)))
    (freshline processor)
    (raw-string processor "@import ")
    (cond
      ((consp url)
       (raw-string processor "url(")
       (raw-string processor (second url))
       (raw-string processor ")"))
      (t (raw-string processor (format nil "\"~a\"" url))))
    (raw-string processor ";")))

(defun process-non-import-css (processor sexp)
  (destructuring-bind (selector &rest attributes) sexp
    (freshline processor)
    (if (selector-group? selector)
	(emit-selector-group processor (cdr selector))
	(emit-selector processor selector))
    (freshline processor)
    (raw-string processor "{")
    (indent processor)
    (freshline processor)
    (process-css-attributes processor attributes)
    (freshline processor)
    (unindent processor)
    (freshline processor)
    (raw-string processor "}")
    (freshline processor)))

(defun process-css-attributes (processor attributes)
  (loop
     with first-item? = t
     for item in attributes
     when (keywordp item) do
       (progn
	 (unless first-item? (raw-string processor ";") (freshline processor))
	 (raw-string processor (subst #\# #\$ (^string item)))
	 (raw-string processor ": ")
	 (setf first-item? nil))
       else do (progn (process-css-key-or-value processor item)
		      (raw-string processor " ")))
  (raw-string processor ";"))




(defun selector-group? (selector)
  (and (consp selector) (eql (car selector) :or)))

(defun selector-descendents? (selector)
  (and (consp selector ) (eql (car selector) :and)))

(defun emit-selector-group (processor selectors)
  (loop with separator = ", "
     for (selector . rest) on selectors
     do (emit-css-selector processor selector)
     when rest do (raw-string processor separator)))

(defun emit-selector-descendents (processor descendents) 
  (loop with separator = " "
     for (descendent . rest) on descendents
     do (emit-css-selector processor descendent)
     when rest do (raw-string processor separator)))

(defun emit-selector-attributes (processor attributes) 
  (loop for (attribute . rest) on attributes
     do (if (atom attribute)
	    (raw-string processor (format nil "[~A]" attribute))
	    (case (car attribute)
	      (_ (raw-string processor (format nil "[~A~~=\"~A\"]" (second attribute) (third attribute))))
	      (- (raw-string processor (format nil "[~A|=\"~A\"]" (second attribute) (third attribute))))
	      (t (raw-string processor (format nil "[~A=\"~A\"]" (first attribute) (second attribute))))))))

(defun extract-pseudo-classes (selector)
  (let ((pseudo-classes (member-if #'keywordp selector)))
    (if (every #'keywordp pseudo-classes)
	(values (ldiff selector pseudo-classes) pseudo-classes)
	(error "pseudo-classes must be at the end of a selector expression: ~S" selector))))

;; (defun emit-selector (processor selector)
;;   (cond
;;     ((atom selector) (raw-string processor (string-invert (string-name selector))))
;;     ((and (consp selector) (member (first selector) '(:and > +)))
;;      ;; TODO :or can only be used one. That is, nested or doens't make sense.
;;      (loop with separator = (case (first selector)
;; 			      (:and " ") (+ " + ") (> " > "))
;;         for (x . rest) on (rest selector)
;;         do (emit-css-selector processor x)
;;         when rest do (raw-string processor separator)))
;;     (t
;;      (multiple-value-bind (selector pseudo-classes)
;; 	 (extract-pseudo-classes selector)
;;        (destructuring-bind (descendents classes attributes id)
;; 	   (split-by-separators selector '(~ @ $)) 
;; 	 (when descendents (emit-selector-descendents processor descendents))
;; 	 (when classes (raw-string processor (format nil "~{.~a~}" (cdr classes))))
;; 	 (emit-selector-attributes processor (cdr attributes))
;; 	 ;; does id go after attributes or before?
;; 	 (when id
;; 	   (and (> (length id) 2) (error "Only one id is allowed for one selector expression ~S" selector))
;; 	   (raw-string processor (format nil "#~a" (second id))))
;; 	 (when pseudo-classes (raw-string processor (format nil "~{:~a~}" pseudo-classes))))))))


(defun emit-selector (processor selector)
  (cond
    ((atom selector) (raw-string processor (substitute #\# #\$ (string-invert (symbol-name selector)))))
    ((and (consp selector) (member (first selector) '(:and > +)))
     ;; TODO :or can only be used one. That is, nested or doens't make sense.
     (loop with separator = (case (first selector)
			      (:and " ") (+ " + ") (> " > "))
        for (x . rest) on (rest selector)
        do (emit-css-selector processor x)
        when rest do (raw-string processor separator)))
    (t (error "Illegal CSS Syntax in ~S" selector))))




(defun parse-selector (selector)
  (if (member (first selector) '(:class :pseudo-class :id))
    (destructuring-bind (&key class pseudo-class id) selector
      (values nil class pseudo-class id))
    (destructuring-bind (tag &key class pseudo-class id) selector
      (values tag class pseudo-class id))))

(defun process-css-key-or-value (processor form)
  (if (keywordp form)
    (raw-string processor (^string form))
    (process *css* processor form nil)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File compiler implementation

(defmethod comment ((language css) text)
  (format nil "~&/*~&~a~&*/" text))

(defmethod top-level-environment ((language css)) nil)
