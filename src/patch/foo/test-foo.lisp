

(in-package :com.gigamonkeys.foo.javascript)
(get-pretty-printer)

(with-foo-output
(process-js '(foo bar BazBaf ~ ab cd @ la boo))

(foo bar BazBaf ~ ab cd @ la boo)
(~ (foo bar BazBaf) ab cd @ la boo)
(@ (~ (foo bar BazBaf) ab cd) la boo)

(process-js 'foo)


(process-js '(%if 1 2 3))

(process-js '(switch 1 (2 3) (4 5)))

(process-js '(switch 1 (2 3) (4 5)))

(process-js '(%return ((function () 3 4 5) ~)))
(process-js '((function () 3) ~))

(process-js '(%return ((fn () 3 4 5) ~)))
(process-js '((fn () 3) ~))

(process-js '(%prog foo 1 2))

(in-package :com.gigamonkeys.foo.lispscript)

(process-ls '(scope ((a b) (c d)) foo 1 2))
(process-ls '(return-last 1 2 a))
(process-ls '(let ((a 10)) 1 2 a))


(process-ls '(dolist (a b) (return-from nil 3)))
(process-ls '(block foo 1 2))
(process-ls '(%prog foo 1 2))
(process-ls '(progn 1 2 3 4))
(process-ls '(block foo 1 (block foo 2 (return-from foo 3)) (return-from foo 4)))

(process-ls '(defun joe (boo) boo))
(process-ls '(defun joe (boo) boo))

(process-ls '(+ 0 (if 1 2 3)))

(process-ls '(xml (:hello jerk face)))
(process-ls '(xml (:b jerk face)))


;; (process-ls '(+ 10 (IF 1 2 3)))

;; (10 + (function () {
;;   if (1) {
;;     return 2;
;;   } else {
;;     return 3;
;;   }
;; }).call(this));

(process-ls '(progn a b c))
;; (function () {
;;   A;
;;   B;
;;   return C;
;; }).call(this);


(process-ls '(return-last a b c))
;; A;
;; B;
;; return C; 
(process-ls '(let ((foo bar)) (foo bar)))

;; (function (FOO) {
;;   return FOO(BAR);
;; }).call(this, BAR);


;; (process-ls '(block foo (block foo2 how happy i am (return-from foo 10)) and you are happy too))
(process-ls '(+ 1 (block boo 1 3 (return-from boo 2) 4)))

;; ((function (block$BOO) {
;;   try {
;;     1;
;;     3;
;;     block$BOO.value = 2;
;;     throw block$BOO;
;;   } catch (g$1) {
;;     if ((g$1 === block$BOO)) return block$BOO.value; else throw g$1;
;;   }
;; }))({ value : undefined });


(in-package :com.gigamonkeys.foo.css)


;; foo[bar][jar~="mar"][dada|="gaga"][qux="deluxe"]



(css ((do ~ re @ mi)
      :padding 40))

;; as a matter of curiosity, this is equivalent to the above
(css ((((((((((((do ~ re @ mi)))))))))))
      :padding 40))


;; ;; when in this form, the last selector expression in the list of descendents must be an atom.
(css (((do ~ re @ mi) fa ~ so @ la :ti :do)
      :padding 40))

;; preferred form 
(css (((do ~ re @ mi) (fa ~ so @ la :ti :do))
      :padding 40))

;; even better
;; ;; this makes longer expressions clearer, but short expressions a little  more cumbersome
;; ;; ;; Maybe I want to enforce this. Maybe.
(css ((:and
       (do ~ re @ mi) (fa ~ so @ la :ti :do))
      :padding 40))

;; wrong
(css (((do ~ re @ mi) (fa ~ bah @ dah :boo :foo) ~ so @ la :ti :do)
      :padding 40))

(css ((:or foo bar)
      :padding 3))

;; error
(css ((:or a b (:or c d))
      :padding 3))


(css ((+ (div ~ c d)
	 (> ((span ~ foo) span)
	    (div :hover)))
      :padding 40))

(css ((+ (div ~ c d)
	 (> ((span ~ foo) span)
	    (div :hover)))
      :padding 40))

(css ((+ (div ~ c d $ id1)
	 (> ((span ~ foo $ id2) span)
	    (div $ id3 :hover)))
      :padding 30))


(css (body :padding "6pt"))
(css (body :padding 6pt))

(css (body :padding 6pt)
     ((:or repl repl2)

      ;; cascading related to fonts
      :font-family courier
      (.some-silly-class
       :font-family comic
       (.more-class :font-color blue))
      
      :overflow scroll
      :height 90%

      ;; cascading related to box
      :border solid thin
      :padding 6pt
      (.some-silly-class :padding 20pt :border dashed red double)
      
      ))

(css (body :padding 6pt)
     ((div.repl div.repl2) ;; and or by default

      ;; cascading related to fonts
      :font-family courier
      (.some-silly-class
       :font-family comic
       (.more-class :font-color blue))
      
      :overflow scroll
      :height 90%

      ;; cascading related to box
      :border solid thin
      :padding 6pt
      (.some-silly-class :padding 20pt :border dashed red double)
      (.some-other-class :padding 0pt)
      
      ))

(css ((:and div div)
      :color black))

;; (emit-css (body padding "6pt")
;; 	  ((:id "repl")
;; 	   font-family courier
;; 	   overflow scroll
;; 	   height "90%"
;; 	   border "solid thin"
;; 	   padding "6pt")
;; 	  ((and (:id "messages") p)
;; 	   background "#ddddff"
;; 	   padding "3pt"
;; 	   margin-top "0pt"
;; 	   margin-bottom "0pt"
;; 	   font-size "12pt")
;; 	  ((and (:id "debugging") p)
;; 	   background "#ffddff"
;; 	   padding "3pt"
;; 	   margin-top "0pt"
;; 	   margin-bottom "0pt"
;; 	   font-size "12pt")
;; 	  ((and (:id "channel-log") p)
;; 	   background "#ffddbb"
;; 	   padding "3pt"
;; 	   margin-top "0pt"
;; 	   margin-bottom "0pt"
;; 	   font-size "12pt")
;; 	  (p padding "0pt")
;; 	  (input w:idth "100%")
;; 	  ((:id arglists)
;; 	   color "#bbbbbb"
;; 	   background "#666666"
;; 	   border "sol:id thin")
;; 	  (.prompt color "#aaaaaa")
;; 	  (.input color "#000000" font-weight bold padding "0pt" padding-top "6pt" margin "0pt")
;; 	  (.result color "#0000ff" padding-top "3pt" margin "0pt")
;; 	  (.output color "#ff00ff" padding-top "3pt" margin "0pt")
;; 	  (.error color "#ff0000" padding-top "3pt" margin "0pt"))



(in-package :com.gigamonkeys.ajax)
(asdf :com.gigamonkeys.ajax)
(start-ajax-server)