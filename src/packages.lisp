
(defpackage :wisp-util
  (:use :cl :unify :cl-ppcre :cl-interpol :arnesi :kmrcl) ; :it.bese.fiveam)
  (:shadowing-import-from :arnesi
			  :copy-file :random-string
			  :quit
			  :compose
			  :it :awhen :aand :acond :aif :acond2)
  ;;(:import-from :arnesi :no-call/cc)
  (:shadowing-import-from :cl-ppcre :quote-meta-chars)
  (:shadowing-import-from :unify
		   :match-case
		   :match)
  #+(or)
  (:shadowing-import-from :kmrcl
			  :it :awhen :aand :acond :aif :awhen2 :aif2 :acond2)
  (:export
   :maptree
   :def-with-mode
   :with
   :with-gensym
   :intersection-in-order
   :split-by-separators 
   :^string
   :^symbol
   :^keyword
   :unquote
   :fn 
   :mklst 
   :strcat
   :symcat 
   :mappend
   :group
   :mvbind
   :mvsetq 
   :dbind
   ;; list
   :last1
   :filter
   :split-if 
   ;;anaphoric macros 
   :self
   :it
   :awhen
   :aif
   :afn
   :aif2
   :awhen2
   :acond2
   :aand
   ;; rkr/macro
   :def/rkr/macro
   :def/rkr/fun
   :rkr/fn
   :&other-keys
   ;; regex
   :=~
   :$
   :$b
   :$m
   :$a
   ;; fmt
   :fmt
   :*fmt-stream*
   ;; kmrcl
   :string-invert
   :list-to-delimited-string
   ;; arnesi
   :curry
   :rcurry
   :conjoin
   :compose
   :eval-always
   ;; reducer/collector
   #:make-reducer
   #:make-pusher
   #:make-collector
   #:with-reducer
   #:with-collector
   #:with-collectors
;;    ;; walker 
   #:walk-form
;;    #:make-walk-env
;;    #:*walk-handlers*
;;    #:*warn-undefined*
;;    #:defwalker-handler
;;    #:implicit-progn-mixin
;;    #:implicit-progn-with-declare-mixin
;;    #:binding-form-mixin
;;    #:declaration-form
;;    #:constant-form
;;    #:variable-reference
;;    #:local-variable-reference
;;    #:local-lexical-variable-reference
;;    #:free-variable-reference
;;    #:application-form
;;    #:local-application-form
;;    #:lexical-application-form
;;    #:free-application-form
;;    #:lambda-application-form
;;    #:function-form
;;    #:lambda-function-form
;;    #:function-object-form
;;    #:local-function-object-form
;;    #:free-function-object-form
;;    #:lexical-function-object-form
;;    #:function-argument-form
;;    #:required-function-argument-form
;;    #:specialized-function-argument-form
;;    #:optional-function-argument-form
;;    #:keyword-function-argument-form
;;    #:allow-other-keys-function-argument-form
;;    #:rest-function-argument-form
;;    #:block-form
;;    #:return-from-form
;;    #:catch-form
;;    #:throw-form
;;    #:eval-when-form
;;    #:if-form
;;    #:function-binding-form
;;    #:flet-form
;;    #:labels-form
;;    #:variable-binding-form
;;    #:let-form
;;    #:let*-form
;;    #:locally-form
;;    #:macrolet-form
;;    #:multiple-value-call-form
;;    #:multiple-value-prog1-form
;;    #:progn-form
;;    #:progv-form
;;    #:setq-form
;;    #:symbol-macrolet-form
;;    #:tagbody-form
;;    #:go-tag-form
;;    #:go-form
;;    #:the-form
;;    #:extract-argument-names
;;    #:walk-lambda-list 
;;    #:unwalk-form 
   ;; call/cc
   #:no-call/cc
   #:to-cps
   #:with-call/cc
   #:kall
   #:call/cc
   #:let/cc
   #:*call/cc-returns*
   #:invalid-return-from
   #:unreachable-code
   #:defun/cc
   #:defgeneric/cc
   #:defmethod/cc
   #:fmakun-cc
   #:*debug-evaluate/cc*
   #:*trace-cc*
   ;; unification
   :unify
   :match
   :match-case
   ))


(defpackage :wisp-sys
  (:use :cl :wisp-util)
  (:export :*wisp-debug*))

(defpackage :wisp-mvc 
  (:use :cl :wisp-util :wisp-sys
	:com.gigamonkeys.foo
	:elephant
;;	:ele-bdb
	:net.aserve 
	:mop
	:cl-ppcre
	:it.bese.fiveam)
  (:export :find-instances
	   :find-instances*
	   
	   :create
	   :destroy
	   :retrieve

	   :print-object-content 
	   ;; wethod, wisplay, wobject. Hahaha... 
	   )
  (:import-from :elephant defpclass))



;; These functions are from KMRCL for displaying a list of symbol values in a package. Don't want them.
(shadow 'show :wisp-mvc)
(shadow 'show-variables :wisp-mvc)
(shadow 'show-functions :wisp-mvc)


;;; Copyright (c) 2006 Howard Yeh
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;;
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;;
;;  - Neither the name of Howard Yeh, nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
